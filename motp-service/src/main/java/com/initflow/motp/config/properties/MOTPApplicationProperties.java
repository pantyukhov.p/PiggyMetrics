package com.initflow.motp.config.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "application.motp", ignoreUnknownFields = false)
public class MOTPApplicationProperties {

    private String motpDefaultURL = "https://ismotp.crptech.ru";
//    "https://stable.ismotp.crptech.ru";

    public String getMotpDefaultURL() {
        return motpDefaultURL;
    }

    public void setMotpDefaultURL(String motpDefaultURL) {
        this.motpDefaultURL = motpDefaultURL;
    }
}
