package com.initflow.motp.service.util;

import com.initflow.motp.domain.MotpOperType;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

public class MOTPLoader {

    private static RestTemplate restTemplate = new RestTemplate();

    private MOTPLoader(){}

    public static <T> T get(String url, Map<String, String> headers, Class<T> zlass){
        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setAll(headers);
        HttpEntity entity = new HttpEntity(httpHeaders);

        ResponseEntity<T> result = restTemplate.exchange(url, HttpMethod.GET,  entity, zlass);
        return result.getBody();
    }

    public static <T,K> T post(String url, K params,  Map<String, String> headers, Class<T> zlass){
        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setAll(headers);
        HttpEntity<K> entity = new HttpEntity<>(params, httpHeaders);

        ResponseEntity<T> result = restTemplate.postForEntity(url, entity, zlass);
        return result.getBody();
    }

    public static <T,K> T getObjectByRequest(MotpOperType type, String url, K params, Map<String, String> headers,
                                             Class<T> zlass){
        headers = headers != null ? headers : new HashMap<>();
        headers.put("Content-Type","application/json");
        switch (type){
            case GET:
                return get(url, headers, zlass);
            case POST:
                return post(url, params, headers, zlass);
        }
        return null;
    }
}
