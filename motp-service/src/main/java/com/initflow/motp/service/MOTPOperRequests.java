package com.initflow.motp.service;

import com.initflow.motp.config.properties.MOTPApplicationProperties;
import com.initflow.motp.domain.MotpOper;
import com.initflow.motp.domain.MotpToken;
import com.initflow.motp.models.motp.loader.*;
import com.initflow.motp.service.domain.MotpOperService;
import com.initflow.motp.service.domain.MotpTokenService;
import com.initflow.motp.service.util.MOTPLoader;
import com.initflow.motp.util.encoder.KriptoProEndcoderUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class MOTPOperRequests {

    @Autowired
    private MotpTokenService motpTokenService;
    @Autowired
    private MotpOperService motpOperService;
    @Autowired
    private MOTPApplicationProperties motpProperties;

    public MotpToken getToken(){
        MotpToken lastToken = motpTokenService.getLastToken();
        if(lastToken != null && lastToken.getValidTo() != null && LocalDateTime.now().isBefore(lastToken.getValidTo())) {
            return lastToken;
        }

        MotpOper authOper = motpOperService.getAuthKeyOper();
        AuthKeyResponce authKeyResp =
                MOTPLoader.getObjectByRequest(authOper.getOperType(),
                        motpProperties.getMotpDefaultURL() + authOper.getOperValue(),null,
                        null, AuthKeyResponce.class);

        if(authKeyResp != null && authKeyResp.data != null && authKeyResp.uuid != null) {
            authKeyResp.data = KriptoProEndcoderUtil.encode(authKeyResp.data);

            MotpOper authSignIn = motpOperService.getAuthSignInKeyOper();
            AuthSignInResponce authSignInResp =
                    MOTPLoader.getObjectByRequest(authSignIn.getOperType(),
                            motpProperties.getMotpDefaultURL() + authSignIn.getOperValue(), authKeyResp,
                            null, AuthSignInResponce.class);

            if(authSignInResp != null && authSignInResp.token != null && authSignInResp.lifetime != null) {
                LocalDateTime validTo = LocalDateTime.now().plusMinutes(authSignInResp.lifetime);
                return motpTokenService.create(authSignInResp.token, validTo);
            } else return null;

        } else return null;
    }

    public AggregatedResponce aggregated(String km){
        MotpToken token = getToken();
        Map<String, String> headers = new HashMap<>();
        headers.put("Authorization","Bearer " + token);

        MotpOper aggrOper = motpOperService.getAggregatedKeyOper();
        String url = motpProperties.getMotpDefaultURL()  + aggrOper.getOperValue() + "?cis=" + km;

        return MOTPLoader.getObjectByRequest(aggrOper.getOperType(), url, null, headers,
                AggregatedResponce.class);
    }

    public CisStatusResponce cisStatus(List<String> ids){
        MotpToken token = getToken();
        Map<String, String> headers = new HashMap<>();
        headers.put("Authorization", "Bearer " + token);

        CisStatusRequestObj reqObj = new CisStatusRequestObj();
        reqObj.ids = ids != null ? ids : new ArrayList<>();

        MotpOper cisOper = motpOperService.getCisStatusKeyOper();
        return MOTPLoader.getObjectByRequest(cisOper.getOperType(),
                motpProperties.getMotpDefaultURL() + cisOper.getOperValue(), reqObj,
                headers, CisStatusResponce.class);
    }

}
