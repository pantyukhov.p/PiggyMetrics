package com.initflow.motp.service.domain;

import com.initflow.base.service.CrudService;
import com.initflow.base.service.ReadAndSaveService;
import com.initflow.motp.domain.MotpOper;
import com.initflow.motp.repository.motp.MotpOperRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Service;

@Service
public class MotpOperService extends CrudService<MotpOper, Long> {

    @Autowired
    private MotpOperRepository repository;

    @Override
    protected PagingAndSortingRepository<MotpOper, Long> getRepository() {
        return repository;
    }

    protected MotpOper getOperByOperId(String operId){
        return repository.getOperByOperId(operId);
    }

    public MotpOper getAuthKeyOper(){
        return getOperByOperId("authkey");
    }

    public MotpOper getAuthSignInKeyOper(){
        return getOperByOperId("authSignIn");
    }

    public MotpOper getAggregatedKeyOper(){
        return getOperByOperId("aggregated");
    }

    public MotpOper getCisStatusKeyOper(){
        return getOperByOperId("cisstatus");
    }




}
