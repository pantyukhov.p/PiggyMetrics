package com.initflow.motp.service.domain;

import com.initflow.base.service.CrudService;
import com.initflow.base.service.ReadAndSaveService;
import com.initflow.motp.domain.MotpToken;
import com.initflow.motp.repository.motp.MotpTokenStorageRepository;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class MotpTokenService extends CrudService<MotpToken, Long> {

    @Autowired
    private MotpTokenStorageRepository repository;

    @Override
    protected PagingAndSortingRepository<MotpToken, Long> getRepository() {
        return repository;
    }

    public MotpToken getLastToken(){
        return repository.findTopByOrderByIdDesc();
    }

    public MotpToken create(String token, LocalDateTime validTo){
        if(!StringUtils.isBlank(token) && validTo != null) {
            MotpToken motpToken = new MotpToken();
            motpToken.setValue(token);
            motpToken.setValidTo(validTo);
            return repository.save(motpToken);
        } else return null;
    }
}
