package com.initflow.motp.domain;

import com.initflow.base.models.domain.BaseEntity;

import javax.persistence.*;

@Entity
@Table(name = "motp_oper")
public class MotpOper extends BaseEntity {

    @Column(length = 40, unique = true)
    private String operId;
    @Column(nullable = false, columnDefinition = "varchar(30) default GET")
    @Enumerated(EnumType.STRING)
    private MotpOperType operType = MotpOperType.GET;
    @Column(length = 400)
    private String operValue;

    public String getOperId() {
        return operId;
    }

    public void setOperId(String operId) {
        this.operId = operId;
    }

    public MotpOperType getOperType() {
        return operType;
    }

    public void setOperType(MotpOperType operType) {
        this.operType = operType;
    }

    public String getOperValue() {
        return operValue;
    }

    public void setOperValue(String operValue) {
        this.operValue = operValue;
    }
}
