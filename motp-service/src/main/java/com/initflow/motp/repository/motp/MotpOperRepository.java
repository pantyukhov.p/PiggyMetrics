package com.initflow.motp.repository.motp;

import com.initflow.motp.domain.MotpOper;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface MotpOperRepository extends PagingAndSortingRepository<MotpOper, Long> {

    @Query("select mo from MotpOper mo where mo.operId = :oper_id")
    MotpOper getOperByOperId(@Param("oper_id") String operId);
}