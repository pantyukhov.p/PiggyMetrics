package com.initflow.motp.repository.motp;

import com.initflow.motp.domain.MotpToken;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MotpTokenStorageRepository extends PagingAndSortingRepository<MotpToken, Long> {

    MotpToken findTopByOrderByIdDesc();
}