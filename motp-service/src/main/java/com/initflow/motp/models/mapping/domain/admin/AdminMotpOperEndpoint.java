package com.initflow.motp.models.mapping.domain.admin;

import com.initflow.motp.domain.MotpOperType;

public class AdminMotpOperEndpoint {

    private Long id;
    private String operId;
    private MotpOperType operType = MotpOperType.GET;
    private String operValue;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOperId() {
        return operId;
    }

    public void setOperId(String operId) {
        this.operId = operId;
    }

    public MotpOperType getOperType() {
        return operType;
    }

    public void setOperType(MotpOperType operType) {
        this.operType = operType;
    }

    public String getOperValue() {
        return operValue;
    }

    public void setOperValue(String operValue) {
        this.operValue = operValue;
    }
}
