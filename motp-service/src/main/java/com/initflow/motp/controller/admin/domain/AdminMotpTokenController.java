package com.initflow.motp.controller.admin.domain;

import com.initflow.base.controller.AbstractCRUDController;
import com.initflow.motp.domain.MotpToken;
import com.initflow.motp.mapper.admin.domain.AdminMotpTokenCrudMapper;
import com.initflow.motp.models.mapping.domain.admin.AdminMotpTokenEndpoint;
import com.initflow.motp.service.domain.MotpTokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.initflow.base.constants.AuthRoles.ADMIN;
import static com.initflow.base.constants.AuthRoles.USER;


@RestController
@RequestMapping("/admin/motp_token")
@PreAuthorize("hasAnyAuthority('"+ ADMIN+ "')")
public class AdminMotpTokenController extends AbstractCRUDController<MotpToken, AdminMotpTokenEndpoint, AdminMotpTokenEndpoint,
        AdminMotpTokenEndpoint, Long> {

    private MotpTokenService mrkhService;
    private AdminMotpTokenCrudMapper docHCrudMapper;

    @Autowired
    public AdminMotpTokenController(MotpTokenService mrkhService, AdminMotpTokenCrudMapper crudMapper){
        super(mrkhService, crudMapper);
        this.mrkhService = mrkhService;
        this.docHCrudMapper = crudMapper;
    }

    @Override
    public String[] getReadRoles() {
        return new String[]{ADMIN, USER};
    }

    @Override
    public String[] getUpdateRoles() {
        return new String[]{ADMIN, USER};
    }

    @Override
    public String[] getCreateRoles() {
        return new String[]{ADMIN, USER};
    }
}