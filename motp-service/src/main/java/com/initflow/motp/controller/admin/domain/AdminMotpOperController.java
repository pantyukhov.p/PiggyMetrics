package com.initflow.motp.controller.admin.domain;

import com.initflow.base.controller.AbstractCRUDController;
import com.initflow.motp.domain.MotpOper;
import com.initflow.motp.mapper.admin.domain.AdminMotpOperCrudMapper;
import com.initflow.motp.models.mapping.domain.admin.AdminMotpOperEndpoint;
import com.initflow.motp.service.domain.MotpOperService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.initflow.base.constants.AuthRoles.ADMIN;
import static com.initflow.base.constants.AuthRoles.USER;

@RestController
@RequestMapping("/admin/motp_oper")
@PreAuthorize("hasAnyAuthority('"+ ADMIN+ "')")
public class AdminMotpOperController extends AbstractCRUDController<MotpOper, AdminMotpOperEndpoint, AdminMotpOperEndpoint,
        AdminMotpOperEndpoint, Long> {

    private MotpOperService mrkhService;
    private AdminMotpOperCrudMapper docHCrudMapper;

    @Autowired
    public AdminMotpOperController(MotpOperService mrkhService, AdminMotpOperCrudMapper crudMapper){
        super(mrkhService, crudMapper);
        this.mrkhService = mrkhService;
        this.docHCrudMapper = crudMapper;
    }

    @Override
    public String[] getReadRoles() {
        return new String[]{ADMIN, USER};
    }

    @Override
    public String[] getUpdateRoles() {
        return new String[]{ADMIN, USER};
    }

    @Override
    public String[] getCreateRoles() {
        return new String[]{ADMIN, USER};
    }
}