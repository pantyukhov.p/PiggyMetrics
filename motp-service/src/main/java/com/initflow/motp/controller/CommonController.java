package com.initflow.motp.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/common")
public class CommonController {


	@RequestMapping("/hello")
	public String hello() {
		return "welcome";
	}

}
