package com.initflow.motp.mapper.admin.domain;

import com.initflow.base.mapper.domain.CrudMapper;
import com.initflow.motp.domain.MotpToken;
import com.initflow.motp.models.mapping.domain.admin.AdminMotpTokenEndpoint;
import org.mapstruct.InheritConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;


@Mapper(componentModel = "spring")
public interface AdminMotpTokenCrudMapper extends CrudMapper<MotpToken, AdminMotpTokenEndpoint, AdminMotpTokenEndpoint,
        AdminMotpTokenEndpoint> {

    @InheritConfiguration
    @Override
    void updateMapping(AdminMotpTokenEndpoint from, @MappingTarget MotpToken to);

}