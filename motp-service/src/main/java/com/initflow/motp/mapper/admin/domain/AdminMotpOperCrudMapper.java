package com.initflow.motp.mapper.admin.domain;

import com.initflow.base.mapper.domain.CrudMapper;
import com.initflow.motp.domain.MotpOper;
import com.initflow.motp.models.mapping.domain.admin.AdminMotpOperEndpoint;
import org.mapstruct.InheritConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;

@Mapper(componentModel = "spring")
public interface AdminMotpOperCrudMapper extends CrudMapper<MotpOper, AdminMotpOperEndpoint, AdminMotpOperEndpoint,
        AdminMotpOperEndpoint> {

    @InheritConfiguration
    @Override
    void updateMapping(AdminMotpOperEndpoint from, @MappingTarget MotpOper to);

}