package com.initflow.base.service;

import com.initflow.base.models.domain.IDObj;

import java.io.Serializable;
import java.util.Optional;
import java.util.function.BiConsumer;
import java.util.function.Function;

public abstract class CrudService<T extends IDObj<ID>, ID extends Serializable> extends ReadAndSaveService<T,ID> {

    public <K> T create(K dto, Function<K,T> mapper){
        T obj = mapper.apply(dto);
        obj.setId(null);
        return save(obj);
    }

    public <K> T update(ID id, K dto, BiConsumer<K, T> mapper){
        Optional<T> obj = findOne(id);
        if(obj.isPresent()){
            T to = obj.get();
            mapper.accept(dto, to);
            to.setId(id);
            return save(to);
        }
        return null;
    }

    public <K> K read(ID id, Function<T,K> mapper){
        Optional<T> obj = findOne(id);
        return obj.map(mapper).orElse(null);
    }
}
