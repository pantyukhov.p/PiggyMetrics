package com.initflow.base.controller;

import com.initflow.base.mapper.domain.CrudMapper;
import com.initflow.base.models.domain.IDObj;
import com.initflow.base.service.CrudService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.Function;
import javax.servlet.http.HttpServletRequest;

public abstract class AbstractCRUDController<T extends IDObj<ID>, C_DTO, U_DTO, R_DTO, ID extends Serializable> {

    private CrudService<T, ID> crudService;
    private CrudMapper<T, C_DTO, U_DTO, R_DTO>  mapper;
//    private CRUDPermission permission;

    public AbstractCRUDController(CrudService<T, ID> crudService, CrudMapper<T, C_DTO, U_DTO, R_DTO>  mapper){
        this.crudService = crudService;
        this.mapper = mapper;
//        this.permission = new DefaultCRUDPermissionImpl();
    }

//    public AbstractCRUDController(CrudService<T, ID> crudService, CrudMapper<T, C_DTO, U_DTO, R_DTO>  mapper,
//                                  CRUDPermission permission){
//        this.crudService = crudService;
//        this.mapper = mapper;
//        this.permission = permission;
//    }

    @PreAuthorize("hasAnyAuthority(#root.this.getReadRoles()) " +
            "and #root.this.getReadPerm(#id, #request, #header)")
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity<R_DTO> read(@PathVariable ID id,
                               HttpServletRequest request, @RequestHeader Map<String, String> header) {
        R_DTO dto = crudService.read(id, getReadMapper());
        return ResponseEntity.ok(dto);
    }

    @PreAuthorize("hasAnyAuthority(#root.this.updateRoles) " +
            "and #root.this.getUpdatePerm(#id, #dto, #request, #header)")
    @RequestMapping(value = "/{id}", method = RequestMethod.PATCH)
    public @ResponseBody
    ResponseEntity<R_DTO> update(@PathVariable ID id, @RequestBody U_DTO dto,
                                 HttpServletRequest request, @RequestHeader Map<String, String> header) {
        T obj = crudService.update(id, dto, getUpdateMapper());
        R_DTO respDTO = getReadMapper().apply(obj);
        return ResponseEntity.ok(respDTO);
    }

    @PreAuthorize("hasAnyAuthority(#root.this.createRoles) " +
            "and #root.this.getCreatePerm(#dto, #request, #header)")
    @RequestMapping(method = RequestMethod.POST)
    public @ResponseBody
    ResponseEntity<R_DTO> create(@RequestBody C_DTO dto,
                                 HttpServletRequest request, @RequestHeader Map<String, String> header) {
        T obj = crudService.create(dto, getCreateMapper());
        R_DTO respDTO = getReadMapper().apply(obj);
        return ResponseEntity.ok(respDTO);
    }

    protected Function<T, R_DTO> getReadMapper() {
        return (T obj) -> mapper.readMapping(obj);
    }

    protected BiConsumer<U_DTO, T> getUpdateMapper() {
        return (U_DTO from, T to) -> mapper.updateMapping(from, to);
    }

    protected Function<C_DTO, T> getCreateMapper() {
        return (C_DTO dto) -> mapper.createMapping(dto);
    }

    public abstract String[] getReadRoles();

    public abstract String[] getUpdateRoles();

    public abstract String[] getCreateRoles();

    public boolean getReadPerm(ID id, HttpServletRequest request, Map<String, String> header){
        return true;
    }

    public boolean getUpdatePerm(ID id, U_DTO dto, HttpServletRequest request, Map<String, String> header){
        return true;
    }

    public boolean getCreatePerm(C_DTO dto, HttpServletRequest request, Map<String, String> header){
        return true;
    }

}
