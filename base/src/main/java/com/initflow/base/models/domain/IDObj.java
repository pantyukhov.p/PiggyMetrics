package com.initflow.base.models.domain;

public interface IDObj<ID> {
    ID getId();
    void setId(ID id);
}
