package com.initflow.base.models.endpoints.mrkdoc;

public class MRKPDocEndpoint {

    private String codemrk;
    private String ebeln;
    private String vbeln;
    private String mblnr;
    private Integer mjahr;

    public MRKPDocEndpoint(){}
    public MRKPDocEndpoint(String codemrk, String ebeln, String vbeln, String mblnr, Integer mjahr){
        this.codemrk = codemrk;
        this.ebeln = ebeln;
        this.vbeln = vbeln;
        this.mblnr = mblnr;
        this.mjahr = mjahr;
    }

    public String getCodemrk() {
        return codemrk;
    }

    public void setCodemrk(String codemrk) {
        this.codemrk = codemrk;
    }

    public String getEbeln() {
        return ebeln;
    }

    public void setEbeln(String ebeln) {
        this.ebeln = ebeln;
    }

    public String getVbeln() {
        return vbeln;
    }

    public void setVbeln(String vbeln) {
        this.vbeln = vbeln;
    }

    public String getMblnr() {
        return mblnr;
    }

    public void setMblnr(String mblnr) {
        this.mblnr = mblnr;
    }

    public Integer getMjahr() {
        return mjahr;
    }

    public void setMjahr(Integer mjahr) {
        this.mjahr = mjahr;
    }
}
