package com.initflow.base.models.endpoints.mrkdoc;

public enum MRKDocEqualStatus {
    EQUAL, MRKP_MORE_ELEMETS, DISCREPANCY
}
