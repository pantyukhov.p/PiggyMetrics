package com.piggymetrics.auth.repository;

import com.piggymetrics.auth.domain.Authority;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AuthorityRepository extends PagingAndSortingRepository<Authority, String> {
}