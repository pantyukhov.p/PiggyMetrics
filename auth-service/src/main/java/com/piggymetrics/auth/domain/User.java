package com.piggymetrics.auth.domain;


import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import javax.persistence.*;
import org.postgresql.Driver;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "mtr_user")
public class User {

	@Id
	private String username;

	private String password;

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(
			name = "mtr_user_auth",
			joinColumns = {@JoinColumn(name = "user_name", referencedColumnName = "username")},
			inverseJoinColumns = {@JoinColumn(name = "auth_name", referencedColumnName = "name")})
	@BatchSize(size = 20)
	private Set<Authority> authorities = new HashSet<>();


	public String getPassword() {
		return password;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Set<Authority> getAuthorities() {
		return authorities;
	}

	public void setAuthorities(Set<Authority> authorities) {
		this.authorities = authorities != null ?  authorities : new HashSet<>();
	}
}
