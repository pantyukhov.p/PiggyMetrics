package com.piggymetrics.auth.service;

import com.piggymetrics.auth.domain.Authority;
import com.piggymetrics.auth.domain.User;
import com.piggymetrics.auth.repository.AuthorityRepository;
import com.piggymetrics.auth.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import javax.transaction.Transactional;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service
public class UserServiceImpl implements UserService {

	private final Logger log = LoggerFactory.getLogger(getClass());

	private static final BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();

	@Autowired
	private UserRepository repository;
	@Autowired
	private AuthorityRepository authorityRepository;

	@Transactional
	@Override
	public void create(User user) {

		Optional<User> existing = repository.findById(user.getUsername());
		existing.ifPresent(it-> {throw new IllegalArgumentException("user already exists: " + it.getUsername());});

		String hash = encoder.encode(user.getPassword());
		user.setPassword(hash);

		appendAuthWithDefaultRoleUser(user);
		repository.save(user);

		log.info("new user has been created: {}", user.getUsername());
	}

	private void appendAuthWithDefaultRoleUser(User user){
		Set<Authority> authorities = new HashSet<>();
		if (user.getAuthorities() != null && user.getAuthorities().size() != 0) {
			user.getAuthorities().forEach(
					auth -> authorityRepository.findById(auth.getName()).ifPresent(authorities::add)
			);
		} else{
			authorityRepository.findById("ROLE_USER").ifPresent(authorities::add);
		}
		user.setAuthorities(authorities);
	}
}
