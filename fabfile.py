#!/usr/bin/env python2

from fabric.api import hide, env, settings, abort, run, cd, shell_env
from fabric.colors import magenta, red
from fabric.contrib.files import append
from fabric.contrib.project import rsync_project
from fabric.operations import run, put
import os

env.user = 'initflow'
env.abort_on_prompts = True
PATH = '/srv/mywebapp'
ENV_FILE = '/srv/mywebapp/.env'

VARIABLES = (
    'CONFIG_SERVICE_PASSWORD',
    'NOTIFICATION_SERVICE_PASSWORD',
    'STATISTICS_SERVICE_PASSWORD',
    'ACCOUNT_SERVICE_PASSWORD',
    'MONGODB_PASSWORD',


    'POSTGRES_USER',
    'POSTGRES_PASSWORD',
    'POSTGRES_DB',
    'POSTGRES_PORT',


    'AUTH_POSTGRES_HOST',
    'ACCOUNT_POSTGRES_HOST',
    'CONFIG_HOST',
    'AUTH_HOST',
    'DOCUMENT_POSTGRES_HOST',
    'MARKING_POSTGRES_HOST',
    'MOTP_POSTGRES_HOST',
    'REGISTRY_HOST',
    'RABBITMQ_HOST',
)


def deploy():
    def docker_compose(command):
        with cd(PATH):
            with shell_env(CI_BUILD_REF_NAME=os.getenv(
                    'CI_BUILD_REF_NAME', 'master')):
                run('set -o pipefail; docker-compose %s | tee' % command)


    variables_set = True
    for var in VARIABLES + ('CI_BUILD_TOKEN',):
        if os.getenv(var) is None:
            variables_set = False
            print(red('ERROR: environment variable ' + var + ' is not set.'))
    if not variables_set:
        abort('Missing required parameters')
    with hide('commands'):
        run('rm -f "%s"' % ENV_FILE)
        append(ENV_FILE,
               ['%s=%s' % (var, val) for var, val in zip(
                   VARIABLES, map(os.getenv, VARIABLES))])

    run('docker login -u %s -p %s %s' % (os.getenv('REGISTRY_USER',
                                                   'gitlab-ci-token'),
                                         os.getenv('CI_BUILD_TOKEN'),
                                         os.getenv('CI_REGISTRY',
                                                   'registry.gitlab.com')))

    put('docker-compose.prod.yml', PATH)
    run('cp '+PATH+'/docker-compose.prod.yml '+PATH+'/docker-compose.yml')
    run('rm  '+PATH+'/docker-compose.prod.yml')
    run('source %s' % ENV_FILE)
    docker_compose("pull")

    # run('docker stop $(docker ps -a -q)')
    # try:
    #     run("docker network disconnect pye_default pye_web_1 -f")
    # except:
    #     pass
    #
    # try:
    #     run("docker network disconnect pye_default $(docker ps -a --filter=\"name=_pye_web\" --format '{{.Names}}') -f")
    # except:
    #     pass

    docker_compose('-p crm up -d')

#
# def deploy():
#     def rsync():
#         exclusions = ('.git*', '.env', '*.sock*', '*.lock', '*.pyc', '*cache*',
#                       '*.log', 'log/', 'id_rsa*', 'maintenance', '**node_modules**',
#                       'web/static/', 'web/app/node_modules/', 'web/static/assets/'  'web/static/**', 'web/app/node_modules/**', 'web/static/assets/**' )
#         rsync_project(PATH, './', exclude=exclusions, delete=True)
#
    # def docker_compose(command):
    #     with cd(PATH):
    #         with shell_env(CI_BUILD_REF_NAME=os.getenv(
    #                 'CI_BUILD_REF_NAME', 'master')):
    #             run('set -o pipefail; docker-compose %s | tee' % command)
#
#     variables_set = True
#     for var in VARIABLES + ('CI_BUILD_TOKEN',):
#         if os.getenv(var) is None:
#             variables_set = False
#             print(red('ERROR: environment variable ' + var + ' is not set.'))
#     if not variables_set:
#         abort('Missing required parameters')
#     with hide('commands'):
#         run('rm -f "%s"' % ENV_FILE)
#         append(ENV_FILE,
#                ['export %s=%s' % (var, val) for var, val in zip(
#                    VARIABLES, map(os.getenv, VARIABLES))])
#
#     run('docker login -u %s -p %s %s' % (os.getenv('REGISTRY_USER',
#                                                    'gitlab-ci-token'),
#                                          os.getenv('CI_BUILD_TOKEN'),
#                                          os.getenv('CI_REGISTRY',
#                                                    'registry.gitlab.com')))
#
#     with settings(warn_only=True):
#         with hide('warnings'):
#             need_bootstrap = run('docker ps | grep -q web').return_code != 0
#     if need_bootstrap:
#         print(magenta('No previous installation found, bootstrapping'))
#         rsync()
#         docker_compose('-p pye up -d')
#
#
#   #  run('touch %s/nginx/maintenance && docker kill -s HUP nginx_1' % PATH)
#     rsync()
#
#     run('source %s' % ENV_FILE)
#     run('echo $DB_NAME')
#     docker_compose('pull')
#     run('docker stop $(docker ps -a -q)')
#     docker_compose('-p pye up -d')
#     with cd(PATH):
#         run('docker exec -it pye_web_1 python manage.py migrate')
#         run('docker exec -it pye_web_1 bash -c "cd app && npm i && npm run build"')
#         run('docker exec -it pye_web_1 python manage.py collectstatic --noinput')
#     #with cd(PATH):
#     #run('sh '+PATH+'/app.sh start')
#    # run('sh '+PATH+'/app.sh collectstatic')
#
#   #  run('rm -f %s/nginx/maintenance && docker kill -s HUP nginx_1' % PATH)
#  #   run('rm -f %s/nginx/maintenance && docker kill -s HUP nginx_1' % PATH)
