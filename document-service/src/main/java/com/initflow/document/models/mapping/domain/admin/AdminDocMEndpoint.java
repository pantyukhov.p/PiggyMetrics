package com.initflow.document.models.mapping.domain.admin;

import com.initflow.document.domain.DocMMarkType;

public class AdminDocMEndpoint {

    private Long id;
    private String codemrk;
    private DocMMarkType mrktype = DocMMarkType.CONSUMER;
//    private AdminDocPEndpoint docP;
    private Long docPId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCodemrk() {
        return codemrk;
    }

    public void setCodemrk(String codemrk) {
        this.codemrk = codemrk;
    }

    public DocMMarkType getMrktype() {
        return mrktype;
    }

    public void setMrktype(DocMMarkType mrktype) {
        this.mrktype = mrktype;
    }

//    public AdminDocPEndpoint getDocP() {
//        return docP;
//    }
//
//    public void setDocP(AdminDocPEndpoint docP) {
//        this.docP = docP;
//    }

    public Long getDocPId() {
        return docPId;
    }

    public void setDocPId(Long docPId) {
        this.docPId = docPId;
    }
}
