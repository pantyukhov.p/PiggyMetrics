package com.initflow.document.models.xmlparser;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import java.util.ArrayList;
import java.util.List;

@JacksonXmlRootElement(localName = "Файл")
public class DocXMLFile {

    @JsonProperty("Документ")
    public Document document;

    public static class Document{
        @JsonProperty("СвСчФакт")
        public SvSchFact svSchFact;

        @JsonProperty("ТаблСчФакт")
        public TablSchFact tablSchFact;
    }

    public static class SvSchFact{
        @JacksonXmlProperty(localName = "ДатаСчФ", isAttribute = true)
        public String dataChF;

    }

    public static class TablSchFact{
        @JacksonXmlProperty(localName = "СведТов")
        @JacksonXmlElementWrapper(useWrapping = false)
        public List<SvedTov> svedTovs = new ArrayList<>();;
    }

    public static class SvedTov{
        @JacksonXmlProperty(localName = "НомСтр", isAttribute = true)
        public String nomStr;

        @JacksonXmlProperty(localName = "НаимТов", isAttribute = true)
        public String naimTov;

        @JacksonXmlProperty(localName = "КолТов", isAttribute = true)
        public Double kolTov;

        @JacksonXmlProperty(localName = "ЦенаТов", isAttribute = true)
        public Double cenaTov;

        @JacksonXmlProperty(localName = "СтТовБезНДС", isAttribute = true)
        public Double stTovBezNDS;

        @JacksonXmlProperty(localName = "НалСт", isAttribute = true)
        public String nalST;

        @JacksonXmlProperty(localName = "СтТовУчНал", isAttribute = true)
        public Double stTovUchNal;

        @JacksonXmlProperty(localName = "ДопСведТов")
        public DopSvedTov dopSvedTov;

        @JacksonXmlProperty(localName = "ИнфПолФХЖ2")
        @JacksonXmlElementWrapper(useWrapping = false)
        public List<InfPolFHJ2> infPolFHJ2s = new ArrayList<>();
    }

    public static class DopSvedTov{
        @JacksonXmlProperty(localName = "НаимЕдИзм", isAttribute = true)
        public String naimEdIzm;
    }

    public static class InfPolFHJ2{

        @JacksonXmlProperty(localName = "Идентиф", isAttribute = true)
        public String identif;

        @JacksonXmlProperty(localName = "Значен", isAttribute = true)
        public String znachen;
    }

}
