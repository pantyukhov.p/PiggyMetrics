package com.initflow.document.models.mapping.domain.admin;

import com.initflow.document.domain.DocDirectionChoices;

public class AdminDocHEndpoint {

    private Long id;
    private DocDirectionChoices direction = DocDirectionChoices.IN;
    private String ebeln;
    private String mblnr;
    private Integer mjahr;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public DocDirectionChoices getDirection() {
        return direction;
    }

    public void setDirection(DocDirectionChoices direction) {
        this.direction = direction;
    }

    public String getEbeln() {
        return ebeln;
    }

    public void setEbeln(String ebeln) {
        this.ebeln = ebeln;
    }

    public String getMblnr() {
        return mblnr;
    }

    public void setMblnr(String mblnr) {
        this.mblnr = mblnr;
    }

    public Integer getMjahr() {
        return mjahr;
    }

    public void setMjahr(Integer mjahr) {
        this.mjahr = mjahr;
    }
}
