package com.initflow.document.models.service.document;

import java.util.HashSet;
import java.util.Set;

public class DocFields {
    private Set<String> codesmrk = new HashSet<>();
    private String ebeln;
    private String vbeln;
    private String mblnr;
    private Integer mjahr;

    public Set<String> getCodesmrk() {
        return codesmrk;
    }

    public void setCodesmrk(Set<String> codesmrk) {
        this.codesmrk = codesmrk;
    }

    public String getEbeln() {
        return ebeln;
    }

    public void setEbeln(String ebeln) {
        this.ebeln = ebeln;
    }

    public String getVbeln() {
        return vbeln;
    }

    public void setVbeln(String vbeln) {
        this.vbeln = vbeln;
    }

    public String getMblnr() {
        return mblnr;
    }

    public void setMblnr(String mblnr) {
        this.mblnr = mblnr;
    }

    public Integer getMjahr() {
        return mjahr;
    }

    public void setMjahr(Integer mjahr) {
        this.mjahr = mjahr;
    }
}
