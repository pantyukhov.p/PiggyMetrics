package com.initflow.document.models.mapping.domain.admin;

public class AdminDocPEndpoint {

    private String posnum;
    private String maktx;
    private Double menge;
    private String meins;
    private Double kbetr;
    private Double dmbtr;
    private Integer nds;
    private Double ndssum;
    private Integer excise;
    private String matcode;
    private String ean;
//    private AdminDocHEndpoint docH;
    private Long docHId;

    public String getPosnum() {
        return posnum;
    }

    public void setPosnum(String posnum) {
        this.posnum = posnum;
    }

    public String getMaktx() {
        return maktx;
    }

    public void setMaktx(String maktx) {
        this.maktx = maktx;
    }

    public Double getMenge() {
        return menge;
    }

    public void setMenge(Double menge) {
        this.menge = menge;
    }

    public String getMeins() {
        return meins;
    }

    public void setMeins(String meins) {
        this.meins = meins;
    }

    public Double getKbetr() {
        return kbetr;
    }

    public void setKbetr(Double kbetr) {
        this.kbetr = kbetr;
    }

    public Double getDmbtr() {
        return dmbtr;
    }

    public void setDmbtr(Double dmbtr) {
        this.dmbtr = dmbtr;
    }

    public Integer getNds() {
        return nds;
    }

    public void setNds(Integer nds) {
        this.nds = nds;
    }

    public Double getNdssum() {
        return ndssum;
    }

    public void setNdssum(Double ndssum) {
        this.ndssum = ndssum;
    }

    public Integer getExcise() {
        return excise;
    }

    public void setExcise(Integer excise) {
        this.excise = excise;
    }

    public String getMatcode() {
        return matcode;
    }

    public void setMatcode(String matcode) {
        this.matcode = matcode;
    }

    public String getEan() {
        return ean;
    }

    public void setEan(String ean) {
        this.ean = ean;
    }

//    public AdminDocHEndpoint getDocH() {
//        return docH;
//    }
//
//    public void setDocH(AdminDocHEndpoint docH) {
//        this.docH = docH;
//    }

    public Long getDocHId() {
        return docHId;
    }

    public void setDocHId(Long docHId) {
        this.docHId = docHId;
    }
}
