package com.initflow.document.service.domain;

import com.initflow.base.service.CrudService;
import com.initflow.document.repository.DocHRepository;
import com.initflow.document.domain.DocH;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class DocHService extends CrudService<DocH, Long> {

    @Autowired
    private DocHRepository repository;

    @Override
    protected PagingAndSortingRepository<DocH, Long> getRepository() {
        return repository;
    }

    public DocH save(DocH obj){
        checkKey(obj);
        return getRepository().save(obj);
    }

    public Iterable<DocH> saveAll(Iterable<DocH> objects){
        objects.forEach(this::checkKey);
        return getRepository().saveAll(objects);
    }

    private void checkKey(DocH obj){
        if(StringUtils.isBlank(obj.getDocnum())){
            obj.setDocnum("uuid_" + UUID.randomUUID().toString());
        }
    }
}
