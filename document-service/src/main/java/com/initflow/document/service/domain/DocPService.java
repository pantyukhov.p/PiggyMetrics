package com.initflow.document.service.domain;

import com.initflow.base.service.CrudService;
import com.initflow.document.domain.DocH;
import com.initflow.document.repository.DocPRepository;
import com.initflow.document.domain.DocP;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Service;

import java.util.UUID;


@Service
public class DocPService extends CrudService<DocP, Long> {

    @Autowired
    private DocPRepository repository;

    @Override
    protected PagingAndSortingRepository<DocP, Long> getRepository() {
        return repository;
    }

    @Override
    public DocP save(DocP obj){
        checkKey(obj);
        return getRepository().save(obj);
    }

    @Override
    public Iterable<DocP> saveAll(Iterable<DocP> objects){
        objects.forEach(this::checkKey);
        return getRepository().saveAll(objects);
    }

    private void checkKey(DocP obj){
        if(StringUtils.isBlank(obj.getPosnum())){
            obj.setPosnum("uuid_" + UUID.randomUUID().toString());
        }
    }
}