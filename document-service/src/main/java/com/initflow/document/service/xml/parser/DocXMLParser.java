package com.initflow.document.service.xml.parser;

import com.initflow.document.domain.DocH;
import com.initflow.document.domain.DocP;
import com.initflow.document.models.xmlparser.DocXMLFile;
import org.apache.commons.lang.StringUtils;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class DocXMLParser {

    private static DateTimeFormatter docDateFormatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");

    private DocXMLParser(){}

    public static DocH parse(DocXMLFile xml){
        DocH docH = parseUPDH(xml);
        List<DocP> docPs = parseUPDP(xml);
        docH.setDocPs(docPs);
        return docH;
    }

    public static DocH parseUPDH(DocXMLFile xml){
        String dataChf = Optional.of(xml).map(it -> it.document).map(it -> it.svSchFact).map(it -> it.dataChF).orElse(null);
        DocH docH = new DocH();
        if(!StringUtils.isBlank(dataChf)){
            LocalDate docDate = LocalDate.parse(dataChf, docDateFormatter);
            docH.setDocdate(docDate.atStartOfDay());
        }
        return docH;
    }

    public static List<DocP> parseUPDP(DocXMLFile xml){
        List<DocP> result = new ArrayList<>();
        List<DocXMLFile.SvedTov> svedTovs = Optional.of(xml).map(it -> it.document).map(it -> it.tablSchFact)
                .map(it -> it.svedTovs).orElse(new ArrayList<>());

        for(DocXMLFile.SvedTov svedTov : svedTovs){
            Integer nds = !StringUtils.isBlank(svedTov.nalST)
                    ? Integer.parseInt(svedTov.nalST.replace("%","")) : null;

            DocP docP = new DocP();
            docP.setPosnum(svedTov.nomStr);
            docP.setMaktx(svedTov.naimTov);
            docP.setMenge(svedTov.kolTov);
            docP.setKbetr(svedTov.cenaTov);
            docP.setDmbtr(svedTov.stTovBezNDS);
            docP.setNds(nds);
            docP.setNdssum(svedTov.stTovUchNal);
            docP.setMeins(Optional.of(svedTov).map(it -> it.dopSvedTov).map(it -> it.naimEdIzm).orElse(null));

            for(DocXMLFile.InfPolFHJ2 infPolFHJ2 : svedTov.infPolFHJ2s){
                if("штриход".equals(infPolFHJ2.identif)){
                    docP.setEan(infPolFHJ2.znachen);
                }
                else if("код_материала".equals(infPolFHJ2.identif)){
                    docP.setMatcode(infPolFHJ2.znachen);
                }

            }
            result.add(docP);
        }
        return result;
    }

}
