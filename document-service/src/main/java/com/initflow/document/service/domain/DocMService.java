package com.initflow.document.service.domain;

import com.initflow.base.models.endpoints.mrkdoc.MRKPDocEndpoint;
import com.initflow.base.service.CrudService;
import com.initflow.document.models.service.document.DocFields;
import com.initflow.document.repository.DocMRepository;
import com.initflow.document.domain.DocM;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;


@Service
public class DocMService extends CrudService<DocM, Long> {

    @Autowired
    private DocMRepository repository;

    @Override
    protected PagingAndSortingRepository<DocM, Long> getRepository() {
        return repository;
    }

    @Transactional(readOnly = true)
    public Set<String> getCodesByJoins(List<MRKPDocEndpoint> codesMrk){
        Set<String> codes = new HashSet<>();
        codesMrk.forEach(it -> codes.addAll(repository.getCodesByJoins(it)));
        return codes;
    }

    @Override
    public DocM save(DocM obj){
        checkKey(obj);
        return getRepository().save(obj);
    }

    @Override
    public Iterable<DocM> saveAll(Iterable<DocM> objects){
        objects.forEach(this::checkKey);
        return getRepository().saveAll(objects);
    }

    private void checkKey(DocM obj){
        if(StringUtils.isBlank(obj.getMrkid())){
            obj.setMrkid("uuid_" + UUID.randomUUID().toString());
        }
    }

    @Transactional(readOnly = true)
    public boolean checkEquals(String docnum1, String docnum2){
        DocFields docFields1 = getCodesByDocnum(docnum1);
        DocFields docFields2 = getCodesByDocnum(docnum2);

        boolean equalFields = Objects.equals(docFields1.getEbeln(), docFields2.getEbeln()) &&
                Objects.equals(docFields1.getMblnr(), docFields2.getMblnr()) &&
                Objects.equals(docFields1.getMjahr(), docFields2.getMjahr()) &&
                Objects.equals(docFields1.getVbeln(), docFields2.getVbeln());

        boolean equalCodesMrk = docFields1.getCodesmrk().size() == docFields2.getCodesmrk().size() &&
                docFields1.getCodesmrk().containsAll(docFields2.getCodesmrk()) &&
                docFields2.getCodesmrk().containsAll(docFields2.getCodesmrk());
        return equalFields && equalCodesMrk;
    }

    public DocFields getCodesByDocnum(String docnum){
        List<Object[]> result = repository.getCodesByDocnum(docnum);
        DocFields docFields = new DocFields();
        if(result.size() > 0){
            docFields.setEbeln((String) result.get(0)[1]);
            docFields.setVbeln((String) result.get(0)[2]);
            docFields.setMblnr((String) result.get(0)[3]);
            docFields.setMjahr((Integer) result.get(0)[4]);
        }
        Set<String> codesMrk = result.stream().map(it -> (String) it[0]).collect(Collectors.toSet());
        docFields.setCodesmrk(codesMrk);
        return docFields;
    }
}