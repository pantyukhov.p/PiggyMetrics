//package com.initflow.document.mapper.domain.user;
//
//import com.initflow.base.mapper.domain.CrudMapper;
//import com.initflow.document.domain.DocP;
//import com.initflow.document.models.mapping.domain.user.UserDocPEndpoint;
//import org.mapstruct.InheritConfiguration;
//import org.mapstruct.Mapper;
//import org.mapstruct.MappingTarget;
//
//
//@Mapper(componentModel = "spring")
//public interface UserDocPCrudMapper extends CrudMapper<DocP, UserDocPEndpoint, UserDocPEndpoint, UserDocPEndpoint> {
//
//    @InheritConfiguration
//    @Override
//    void updateMapping(UserDocPEndpoint from, @MappingTarget DocP to);
//
//}
