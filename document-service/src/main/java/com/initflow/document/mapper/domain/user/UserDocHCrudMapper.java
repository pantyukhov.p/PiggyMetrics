//package com.initflow.document.mapper.domain.user;
//
//import com.initflow.base.mapper.domain.CrudMapper;
//import com.initflow.document.domain.DocH;
//import com.initflow.document.models.mapping.domain.user.UserDocHEndpoint;
//import org.mapstruct.InheritConfiguration;
//import org.mapstruct.Mapper;
//import org.mapstruct.MappingTarget;
//
//@Mapper(componentModel = "spring")
//public interface UserDocHCrudMapper extends CrudMapper<DocH, UserDocHEndpoint, UserDocHEndpoint, UserDocHEndpoint> {
//
//    @InheritConfiguration
//    @Override
//    void updateMapping(UserDocHEndpoint from, @MappingTarget DocH to);
//
//}