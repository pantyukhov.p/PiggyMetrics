package com.initflow.document.mapper.domain.admin;

import com.initflow.base.mapper.domain.CrudMapper;
import com.initflow.document.domain.DocP;
import com.initflow.document.models.mapping.domain.admin.AdminDocPEndpoint;
import org.mapstruct.*;

@Mapper(componentModel = "spring", uses = {AdminDocMCrudMapper.class})
public interface AdminDocPCrudMapper extends CrudMapper<DocP, AdminDocPEndpoint, AdminDocPEndpoint, AdminDocPEndpoint> {

    @InheritConfiguration
    @Override
    @Mappings({
            @Mapping(target = "docH.id", source = "docHId")
    })
    void updateMapping(AdminDocPEndpoint from, @MappingTarget DocP to);

    @Override
    @Mappings({
            @Mapping(target = "docH.id", source = "docHId")
    })
    DocP createMapping(AdminDocPEndpoint obj);

    @Override
    @Mappings({
            @Mapping(target = "docHId", source = "docH.id")
    })
    AdminDocPEndpoint readMapping(DocP obj);
}