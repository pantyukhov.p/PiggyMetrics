//package com.initflow.document.mapper.domain.user;
//
//import com.initflow.base.mapper.domain.CrudMapper;
//import com.initflow.document.domain.DocM;
//import com.initflow.document.models.mapping.domain.user.UserDocMEndpoint;
//import org.mapstruct.InheritConfiguration;
//import org.mapstruct.Mapper;
//import org.mapstruct.MappingTarget;
//
//
//@Mapper(componentModel = "spring")
//public interface UserDocMCrudMapper extends CrudMapper<DocM, UserDocMEndpoint, UserDocMEndpoint, UserDocMEndpoint> {
//
//    @InheritConfiguration
//    @Override
//    void updateMapping(UserDocMEndpoint from, @MappingTarget DocM to);
//
//}