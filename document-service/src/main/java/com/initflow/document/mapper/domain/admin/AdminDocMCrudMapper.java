package com.initflow.document.mapper.domain.admin;

import com.initflow.base.mapper.domain.CrudMapper;
import com.initflow.document.domain.DocM;
import com.initflow.document.models.mapping.domain.admin.AdminDocMEndpoint;
import org.mapstruct.*;

@Mapper(componentModel = "spring", uses = {AdminDocHCrudMapper.class})
public interface AdminDocMCrudMapper extends CrudMapper<DocM, AdminDocMEndpoint, AdminDocMEndpoint, AdminDocMEndpoint> {

    @InheritConfiguration
    @Override
    @Mappings({
            @Mapping(target = "docP.id", source = "docPId")
    })
    void updateMapping(AdminDocMEndpoint from, @MappingTarget DocM to);


    @Override
    @Mappings({
            @Mapping(target = "docP.id", source = "docPId")
    })
    DocM createMapping(AdminDocMEndpoint obj);

    @Override
    @Mappings({
            @Mapping(target = "docPId", source = "docP.id")
    })
    AdminDocMEndpoint readMapping(DocM obj);

}