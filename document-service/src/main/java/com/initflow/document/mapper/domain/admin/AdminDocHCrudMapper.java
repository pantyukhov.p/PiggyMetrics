package com.initflow.document.mapper.domain.admin;

import com.initflow.base.mapper.domain.CrudMapper;
import com.initflow.document.domain.DocH;
import com.initflow.document.models.mapping.domain.admin.AdminDocHEndpoint;
import org.mapstruct.InheritConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;

@Mapper(componentModel = "spring")
public interface AdminDocHCrudMapper extends CrudMapper<DocH, AdminDocHEndpoint, AdminDocHEndpoint, AdminDocHEndpoint> {

    @InheritConfiguration
    @Override
    void updateMapping(AdminDocHEndpoint from, @MappingTarget DocH to);

}
