package com.initflow.document.repository;

import com.initflow.base.models.endpoints.mrkdoc.MRKPDocEndpoint;
import com.initflow.document.domain.DocM;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DocMRepository extends PagingAndSortingRepository<DocM, Long> {

    @Query("select m.codemrk from DocM m join DocH h " +
            "on h.docnum = m.docP.docH.docnum and " +
            "h.ebeln = :#{#codesMrk.ebeln} and " +
            "h.vbeln = :#{#codesMrk.vbeln} and " +
            "h.mblnr = :#{#codesMrk.mblnr} and " +
            "h.mjahr = :#{#codesMrk.mjahr} ")
    List<String> getCodesByJoins(@Param("codesMrk") MRKPDocEndpoint codesMrk);

    @Query("select m.codemrk, h.ebeln, h.vbeln, h.mblnr, h.mjahr from DocM m join DocH h " +
            "on h.docnum = m.docP.docH.docnum " +
            "where m.docP.docH.docnum = :docnum")
    List<Object[]> getCodesByDocnum(@Param("docnum") String docnum);

}