package com.initflow.document.repository;

import com.initflow.document.domain.DocH;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DocHRepository extends PagingAndSortingRepository<DocH, Long> {
}
