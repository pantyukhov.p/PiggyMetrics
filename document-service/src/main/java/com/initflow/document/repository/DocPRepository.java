package com.initflow.document.repository;

import com.initflow.document.domain.DocP;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DocPRepository extends PagingAndSortingRepository<DocP, Long> {
}