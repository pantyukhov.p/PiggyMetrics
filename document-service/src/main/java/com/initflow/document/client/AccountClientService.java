package com.initflow.document.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import java.util.Set;

@FeignClient(name = "account-service")
public interface AccountClientService {

    @RequestMapping(method = RequestMethod.POST, value = "/accounts/current/companies",
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    Set<String> getCompanies();

}