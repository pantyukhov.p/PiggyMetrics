package com.initflow.document.controller.admin.mrk;

import com.initflow.base.models.endpoints.mrkdoc.MRKDocEqualStatus;
import com.initflow.base.models.endpoints.mrkdoc.MRKPDocEndpoint;
import com.initflow.document.service.domain.DocMService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static com.initflow.base.constants.AuthRoles.ADMIN;

@RestController
@RequestMapping("/admin/doc/mrk")
//@PreAuthorize("hasAnyAuthority('"+ ADMIN+ "')")
public class AdminMRKController {

    @Autowired
    private DocMService docMService;

//    @PreAuthorize("hasAnyAuthority('"+ ADMIN+ "')")
    @RequestMapping(value = "/check", method = RequestMethod.POST)
    public @ResponseBody
    ResponseEntity<MRKDocEqualStatus> checkEqualCodesMrk(@NotNull @RequestBody List<MRKPDocEndpoint> codesMrk) {
        Set<String> codesMRKValues = codesMrk.stream().map(it -> it.getCodemrk()).collect(Collectors.toSet());
        Set<String> codesMRKByDoc = docMService.getCodesByJoins(codesMrk);

        boolean codesMrkContainsAll = codesMRKValues.containsAll(codesMRKByDoc);
        boolean codesMrkByDocContainsAll = codesMRKByDoc.containsAll(codesMRKValues);
        if(codesMrkContainsAll && codesMrkByDocContainsAll){
            return ResponseEntity.ok(MRKDocEqualStatus.EQUAL);
        } else if(!codesMrkByDocContainsAll) {
            return ResponseEntity.ok(MRKDocEqualStatus.MRKP_MORE_ELEMETS);
        } else {
            return ResponseEntity.ok(MRKDocEqualStatus.DISCREPANCY);
        }
    }
}