package com.initflow.document.controller.admin;

import com.initflow.document.domain.DocH;
import com.initflow.document.domain.DocP;
import com.initflow.document.models.xmlparser.DocXMLFile;
import com.initflow.document.service.domain.DocHService;
import com.initflow.document.service.domain.DocPService;
import com.initflow.document.service.xml.parser.DocXMLParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/admin/doc")
public class LoadDataController {

    @Autowired
    private DocHService updhService;
    @Autowired
    private DocPService updpService;

    @PostMapping(value = "/load", consumes = MediaType.APPLICATION_XML_VALUE)
    public String loadUPD(@RequestBody DocXMLFile xmlValue) {
        DocH docH = DocXMLParser.parse(xmlValue);
        docH = updhService.save(docH);
        for(DocP docP : docH.getDocPs()){
            docP.setDocH(docH);
        }
        Iterable<DocP> docpList = updpService.saveAll(docH.getDocPs());
        return "welcome";
    }

}

