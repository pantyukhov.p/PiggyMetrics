package com.initflow.document.controller.admin.domain;

import com.initflow.base.controller.AbstractCRUDController;
import com.initflow.document.domain.DocM;
import com.initflow.document.mapper.domain.admin.AdminDocMCrudMapper;
import com.initflow.document.models.mapping.domain.admin.AdminDocMEndpoint;
import com.initflow.document.service.domain.DocMService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.initflow.base.constants.AuthRoles.ADMIN;
import static com.initflow.base.constants.AuthRoles.USER;

@RestController
@RequestMapping("/admin/doc_m")
@PreAuthorize("AdminDocMController('"+ ADMIN+ "')")
public class AdminDocMController extends AbstractCRUDController<DocM, AdminDocMEndpoint, AdminDocMEndpoint,
        AdminDocMEndpoint, Long> {

    private DocMService docMService;
    private AdminDocMCrudMapper docMCrudMapper;

    @Autowired
    public AdminDocMController(DocMService docMService, AdminDocMCrudMapper crudMapper){
        super(docMService, crudMapper);
        this.docMService = docMService;
        this.docMCrudMapper = crudMapper;
    }

    @Override
    public String[] getReadRoles() {
        return new String[]{ADMIN, USER};
    }

    @Override
    public String[] getUpdateRoles() {
        return new String[]{ADMIN, USER};
    }

    @Override
    public String[] getCreateRoles() {
        return new String[]{ADMIN, USER};
    }
}
