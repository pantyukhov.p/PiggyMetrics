package com.initflow.document.controller.admin.domain;

import com.initflow.base.controller.AbstractCRUDController;
import com.initflow.document.domain.DocP;
import com.initflow.document.mapper.domain.admin.AdminDocPCrudMapper;
import com.initflow.document.models.mapping.domain.admin.AdminDocPEndpoint;
import com.initflow.document.service.domain.DocPService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.initflow.base.constants.AuthRoles.ADMIN;
import static com.initflow.base.constants.AuthRoles.USER;


@RestController
@RequestMapping("/admin/doc_p")
@PreAuthorize("hasAnyAuthority('"+ ADMIN+ "')")
public class AdminDocPController extends AbstractCRUDController<DocP, AdminDocPEndpoint, AdminDocPEndpoint,
        AdminDocPEndpoint, Long> {

    private DocPService docPService;
    private AdminDocPCrudMapper docPCrudMapper;

    @Autowired
    public AdminDocPController(DocPService docPService, AdminDocPCrudMapper crudMapper){
        super(docPService, crudMapper);
        this.docPService = docPService;
        this.docPCrudMapper = crudMapper;
    }

    @Override
    public String[] getReadRoles() {
        return new String[]{ADMIN, USER};
    }

    @Override
    public String[] getUpdateRoles() {
        return new String[]{ADMIN, USER};
    }

    @Override
    public String[] getCreateRoles() {
        return new String[]{ADMIN, USER};
    }
}
