package com.initflow.document.controller.admin.domain;

import com.initflow.base.controller.AbstractCRUDController;
import com.initflow.base.models.endpoints.mrkdoc.MRKDocEqualStatus;
import com.initflow.base.models.endpoints.mrkdoc.MRKPDocEndpoint;
import com.initflow.document.client.AccountClientService;
import com.initflow.document.domain.DocH;
import com.initflow.document.mapper.domain.admin.AdminDocHCrudMapper;
import com.initflow.document.models.mapping.domain.admin.AdminDocHEndpoint;
import com.initflow.document.service.domain.DocHService;
import com.initflow.document.service.domain.DocMService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import static com.initflow.base.constants.AuthRoles.ADMIN;
import static com.initflow.base.constants.AuthRoles.USER;

@RestController
@RequestMapping("/admin/doc_h")
//@PreAuthorize("hasAnyAuthority('"+ ADMIN+ "')")
public class AdminDocHController extends AbstractCRUDController<DocH, AdminDocHEndpoint, AdminDocHEndpoint,
        AdminDocHEndpoint, Long> {

    private DocHService docHService;
    private AdminDocHCrudMapper docHCrudMapper;
    @Autowired
    private AccountClientService accountClientService;
    @Autowired
    private DocMService docMService;

    @Autowired
    public AdminDocHController(DocHService docHService, AdminDocHCrudMapper crudMapper) {
        super(docHService, crudMapper);
        this.docHService = docHService;
        this.docHCrudMapper = crudMapper;
    }

    @Override
    public String[] getReadRoles() {
        return new String[]{ADMIN, USER};
    }

    @Override
    public String[] getUpdateRoles() {
        return new String[]{ADMIN, USER};
    }

    @Override
    public String[] getCreateRoles() {
        return new String[]{ADMIN, USER};
    }

    @Override
    public boolean getReadPerm(Long id, HttpServletRequest request, Map<String, String> header){
        Set<String> companies = accountClientService.getCompanies();
        Optional<DocH> docH = docHService.findOne(id);
        String companyName = docH.map(it -> it.getCompany().getUniqName()).orElse(null);
        return companies.contains(companyName);
    }

    @Override
    public boolean getUpdatePerm(Long id, AdminDocHEndpoint dto, HttpServletRequest request, Map<String, String> header){
        return true;
    }

    @Override
    public boolean getCreatePerm(AdminDocHEndpoint dto, HttpServletRequest request, Map<String, String> header){
        return true;
    }

    @RequestMapping(value = "/equal", method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity<Boolean> checkEqualDoc(@RequestParam String docnum1, @RequestParam String docnum2) {
        return ResponseEntity.ok(docMService.checkEquals(docnum1, docnum2));
    }
}
