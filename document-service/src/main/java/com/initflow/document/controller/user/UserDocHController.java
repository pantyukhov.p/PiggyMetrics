//package com.initflow.document.controller.user;
//
//import com.initflow.base.controller.AbstractCRUDController;
//import com.initflow.document.domain.DocH;
//import com.initflow.document.mapper.domain.admin.AdminDocHCrudMapper;
//import com.initflow.document.mapper.domain.user.UserDocHCrudMapper;
//import com.initflow.document.models.mapping.domain.user.UserDocHEndpoint;
//import com.initflow.document.service.domain.DocHService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.access.prepost.PreAuthorize;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
//import static com.initflow.base.constants.AuthRoles.USER;
//
//@RestController
//@RequestMapping("/user/doc_h")
//@PreAuthorize("hasAnyAuthority('"+ USER+ "')")
//public class UserDocHController extends AbstractCRUDController<DocH, UserDocHEndpoint, UserDocHEndpoint,
//        UserDocHEndpoint, Long> {
//
//    private DocHService docHService;
//    private UserDocHCrudMapper docHCrudMapper;
//
//    @Autowired
//    public UserDocHController(DocHService docHService, UserDocHCrudMapper crudMapper){
//        super(docHService, crudMapper);
//        this.docHService = docHService;
//        this.docHCrudMapper = crudMapper;
//    }
//
//}