//package com.initflow.document.controller.user;
//
//import com.initflow.base.controller.AbstractCRUDController;
//import com.initflow.document.domain.DocM;
//import com.initflow.document.mapper.domain.user.UserDocMCrudMapper;
//import com.initflow.document.models.mapping.domain.user.UserDocMEndpoint;
//import com.initflow.document.service.domain.DocMService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.access.prepost.PreAuthorize;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
//import static com.initflow.base.constants.AuthRoles.USER;
//
////@RestController
////@RequestMapping("/user/doc_m")
////@PreAuthorize("hasAnyAuthority('"+ USER+ "')")
//public class UserDocMController extends AbstractCRUDController<DocM, UserDocMEndpoint, UserDocMEndpoint,
//        UserDocMEndpoint, Long> {
//
//    private DocMService docMService;
//    private UserDocMCrudMapper docMCrudMapper;
//
//    @Autowired
//    public UserDocMController(DocMService docMService, UserDocMCrudMapper crudMapper){
//        super(docMService, crudMapper);
//        this.docMService = docMService;
//        this.docMCrudMapper = crudMapper;
//    }
//
//}