//package com.initflow.document.controller.user;
//
//import com.initflow.base.controller.AbstractCRUDController;
//import com.initflow.document.domain.DocH;
//import com.initflow.document.domain.DocP;
//import com.initflow.document.mapper.domain.user.UserDocHCrudMapper;
//import com.initflow.document.mapper.domain.user.UserDocPCrudMapper;
//import com.initflow.document.models.mapping.domain.user.UserDocHEndpoint;
//import com.initflow.document.models.mapping.domain.user.UserDocPEndpoint;
//import com.initflow.document.service.domain.DocHService;
//import com.initflow.document.service.domain.DocPService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.access.prepost.PreAuthorize;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
//import static com.initflow.base.constants.AuthRoles.USER;
//
////@RestController
////@RequestMapping("/user/doc_p")
////@PreAuthorize("hasAnyAuthority('"+ USER+ "')")
//public class UserDocPController extends AbstractCRUDController<DocP, UserDocPEndpoint, UserDocPEndpoint,
//        UserDocPEndpoint, Long> {
//
//    private DocPService docHService;
//    private UserDocPCrudMapper docHCrudMapper;
//
//    @Autowired
//    public UserDocPController(DocPService docHService, UserDocPCrudMapper crudMapper){
//        super(docHService, crudMapper);
//        this.docHService = docHService;
//        this.docHCrudMapper = crudMapper;
//    }
//
//}