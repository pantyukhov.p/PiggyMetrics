package com.initflow.document.domain;

import com.initflow.base.models.domain.BaseEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;

@Entity
@Table(name = "doc_company", indexes = {@Index(name = "doc_company_uniq_name_index", columnList = "uniq_name", unique = true)})
public class Company extends BaseEntity {

    @Column(name = "uniq_name", unique = true, nullable = false)
    private String uniqName;
    @Column(name = "name", nullable = false)
    private String name;


    public String getUniqName() {
        return uniqName;
    }

    public void setUniqName(String uniqName) {
        this.uniqName = uniqName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
