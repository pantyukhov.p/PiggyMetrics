package com.initflow.document.domain;

public enum DocMMarkType {
    CONSUMER(1, "Потребительская (CIS)"), GROUP(2, "Групповая(CIS)"),
    TRANSPORTATION(3,"Tранспортная(SSCC)");

    private String value;
    private int index;

    DocMMarkType(int index, String value){
        this.index = index;
        this.value = value;
    }

    public int getIndex() {
        return index;
    }

    public String getValue() {
        return value;
    }
}
