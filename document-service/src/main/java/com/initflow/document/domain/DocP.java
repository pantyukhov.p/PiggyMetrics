package com.initflow.document.domain;

import com.initflow.base.models.domain.BaseEntity;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "doc_p", indexes = {@Index(name = "doc_p_posnum_index", columnList = "posnum", unique = true)})
public class DocP extends BaseEntity {

    @Column(length = 50, unique = true, nullable = false)
    private String posnum;
    @Column(length = 300)
    private String maktx;
    @Column
    private Double menge;
    @Column(length = 50)
    private String meins;
    @Column
    private Double kbetr;
    @Column
    private Double dmbtr;
    @Column
    private Integer nds;
    @Column
    private Double ndssum;
    @Column
    private Integer excise;
    @Column(length = 50)
    private String matcode;
    @Column(length = 50)
    private String ean;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "doc_h_id")
    private DocH docH;

    @OneToMany(mappedBy = "docP")
    private List<DocM> docMs = new ArrayList<>();

    public String getPosnum() {
        return posnum;
    }

    public void setPosnum(String posnum) {
        this.posnum = posnum;
    }

    public String getMaktx() {
        return maktx;
    }

    public void setMaktx(String maktx) {
        this.maktx = maktx;
    }

    public Double getMenge() {
        return menge;
    }

    public void setMenge(Double menge) {
        this.menge = menge;
    }

    public String getMeins() {
        return meins;
    }

    public void setMeins(String meins) {
        this.meins = meins;
    }

    public Double getKbetr() {
        return kbetr;
    }

    public void setKbetr(Double kbetr) {
        this.kbetr = kbetr;
    }

    public Double getDmbtr() {
        return dmbtr;
    }

    public void setDmbtr(Double dmbtr) {
        this.dmbtr = dmbtr;
    }

    public Integer getNds() {
        return nds;
    }

    public void setNds(Integer nds) {
        this.nds = nds;
    }

    public Double getNdssum() {
        return ndssum;
    }

    public void setNdssum(Double ndssum) {
        this.ndssum = ndssum;
    }

    public Integer getExcise() {
        return excise;
    }

    public void setExcise(Integer excise) {
        this.excise = excise;
    }

    public String getMatcode() {
        return matcode;
    }

    public void setMatcode(String matcode) {
        this.matcode = matcode;
    }

    public String getEan() {
        return ean;
    }

    public void setEan(String ean) {
        this.ean = ean;
    }

    public DocH getDocH() {
        return docH;
    }

    public void setDocH(DocH docH) {
        this.docH = docH;
    }

    public List<DocM> getDocMs() {
        return docMs;
    }

    public void setDocMs(List<DocM> docMs) {
        this.docMs = docMs;
    }
}
