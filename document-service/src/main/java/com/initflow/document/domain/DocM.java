package com.initflow.document.domain;

import com.initflow.base.models.domain.BaseEntity;

import javax.persistence.*;

@Entity
@Table(name = "doc_m", indexes = {@Index(name = "doc_m_mrkid_index", columnList = "mrkid", unique = true)})
public class DocM extends BaseEntity {

    @Column(name = "mrkid", length = 200, unique = true, nullable = false)
    private String mrkid;
    @Column(length = 200)
    private String codemrk;

    @Column(nullable = false, columnDefinition = "varchar(30) default CONSUMER")
    @Enumerated(EnumType.STRING)
    private DocMMarkType mrktype = DocMMarkType.CONSUMER;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "doc_p_id")
    private DocP docP;

    public String getCodemrk() {
        return codemrk;
    }

    public void setCodemrk(String codemrk) {
        this.codemrk = codemrk;
    }

    public DocMMarkType getMrktype() {
        return mrktype;
    }

    public void setMrktype(DocMMarkType mrktype) {
        this.mrktype = mrktype;
    }

    public DocP getDocP() {
        return docP;
    }

    public void setDocP(DocP docP) {
        this.docP = docP;
    }

    public String getMrkid() {
        return mrkid;
    }

    public void setMrkid(String mrkid) {
        this.mrkid = mrkid;
    }
}
