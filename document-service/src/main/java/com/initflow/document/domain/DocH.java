package com.initflow.document.domain;

import com.initflow.base.models.domain.BaseEntity;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;


@Entity
@Table(name = "doc_h", indexes = {@Index(name = "doc_h_docnum_index", columnList = "docnum", unique = true)})
public class DocH extends BaseEntity {


    @Column(nullable = false, unique = true, length = 50)
    private String docnum;
    @Column(nullable = false, columnDefinition = "varchar(30) default IN")
    @Enumerated(EnumType.STRING)
    private DocDirectionChoices direction = DocDirectionChoices.IN;
    @Column(columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    private LocalDateTime docdate;
    @Column(length = 50)
    private String ebeln;
    @Column(length = 50)
    private String vbeln;
    @Column(length = 50)
    private String mblnr;
    @Column
    private Integer mjahr;
    @OneToMany(mappedBy = "docH")
    private List<DocP> docPs = new ArrayList<>();

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "company_id")
    private Company company;

    public DocDirectionChoices getDirection() {
        return direction;
    }

    public void setDirection(DocDirectionChoices direction) {
        this.direction = direction;
    }

    public LocalDateTime getDocdate() {
        return docdate;
    }

    public void setDocdate(LocalDateTime docdate) {
        this.docdate = docdate;
    }

    public String getEbeln() {
        return ebeln;
    }

    public void setEbeln(String ebeln) {
        this.ebeln = ebeln;
    }

    public String getMblnr() {
        return mblnr;
    }

    public void setMblnr(String mblnr) {
        this.mblnr = mblnr;
    }

    public Integer getMjahr() {
        return mjahr;
    }

    public void setMjahr(Integer mjahr) {
        this.mjahr = mjahr;
    }

    public List<DocP> getDocPs() {
        return docPs;
    }

    public void setDocPs(List<DocP> docPs) {
        this.docPs = docPs;
    }

    public String getVbeln() {
        return vbeln;
    }

    public void setVbeln(String vbeln) {
        this.vbeln = vbeln;
    }

    public String getDocnum() {
        return docnum;
    }

    public void setDocnum(String docnum) {
        this.docnum = docnum;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }
}
