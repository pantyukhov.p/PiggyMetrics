package com.initflow.document.domain;

public enum DocDirectionChoices {
    IN("Входящий"), OUT("Исходящий");

    private String value;

    DocDirectionChoices(String value){
        this.value = value;
    }

    public String getValue(){
        return value;
    }
}
