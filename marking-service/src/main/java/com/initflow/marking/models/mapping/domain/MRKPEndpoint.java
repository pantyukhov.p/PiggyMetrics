package com.initflow.marking.models.mapping.domain;

import com.initflow.marking.domain.marking.MRKPStatusesChoices;
import com.initflow.marking.domain.marking.MarkingTypeOfPackaging;

public class MRKPEndpoint {

    private Long id;
    private String posnum;
    private String codemrk;
    private MarkingTypeOfPackaging mrktype = MarkingTypeOfPackaging.CONSUMER;
    private String ebeln;
    private Integer ebelp;
    private String vbeln;
    private Integer posnr;
    private String mblnr;
    private Integer mjahr;
    private MRKPStatusesChoices posstatus = MRKPStatusesChoices.MRKP_STATUSE_OK;

    private Long mrkhId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPosnum() {
        return posnum;
    }

    public void setPosnum(String posnum) {
        this.posnum = posnum;
    }

    public String getCodemrk() {
        return codemrk;
    }

    public void setCodemrk(String codemrk) {
        this.codemrk = codemrk;
    }

    public MarkingTypeOfPackaging getMrktype() {
        return mrktype;
    }

    public void setMrktype(MarkingTypeOfPackaging mrktype) {
        this.mrktype = mrktype;
    }

    public String getEbeln() {
        return ebeln;
    }

    public void setEbeln(String ebeln) {
        this.ebeln = ebeln;
    }

    public Integer getEbelp() {
        return ebelp;
    }

    public void setEbelp(Integer ebelp) {
        this.ebelp = ebelp;
    }

    public String getVbeln() {
        return vbeln;
    }

    public void setVbeln(String vbeln) {
        this.vbeln = vbeln;
    }

    public Integer getPosnr() {
        return posnr;
    }

    public void setPosnr(Integer posnr) {
        this.posnr = posnr;
    }

    public String getMblnr() {
        return mblnr;
    }

    public void setMblnr(String mblnr) {
        this.mblnr = mblnr;
    }

    public Integer getMjahr() {
        return mjahr;
    }

    public void setMjahr(Integer mjahr) {
        this.mjahr = mjahr;
    }

    public MRKPStatusesChoices getPosstatus() {
        return posstatus;
    }

    public void setPosstatus(MRKPStatusesChoices posstatus) {
        this.posstatus = posstatus;
    }

    public Long getMrkhId() {
        return mrkhId;
    }

    public void setMrkhId(Long mrkhId) {
        this.mrkhId = mrkhId;
    }
}
