package com.initflow.marking.models.mapping.domain.admin;

import com.initflow.marking.domain.marking.MarkingDirectionChoices;
import com.initflow.marking.domain.marking.MarkingStatusesChoices;

import java.time.LocalDateTime;

public class AdminMRKHEndpoint {

    private Long id;
    private MarkingDirectionChoices direction = MarkingDirectionChoices.IN;
    private LocalDateTime docDate;
    private String lifnr;
    private String ebeln;
    private String vbeln;
    private String mblnr;
    private String mjahr;
    private MarkingStatusesChoices docStatus = MarkingStatusesChoices.STATUSE_OK;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public MarkingDirectionChoices getDirection() {
        return direction;
    }

    public void setDirection(MarkingDirectionChoices direction) {
        this.direction = direction;
    }

    public LocalDateTime getDocDate() {
        return docDate;
    }

    public void setDocDate(LocalDateTime docDate) {
        this.docDate = docDate;
    }

    public String getLifnr() {
        return lifnr;
    }

    public void setLifnr(String lifnr) {
        this.lifnr = lifnr;
    }

    public String getEbeln() {
        return ebeln;
    }

    public void setEbeln(String ebeln) {
        this.ebeln = ebeln;
    }

    public String getVbeln() {
        return vbeln;
    }

    public void setVbeln(String vbeln) {
        this.vbeln = vbeln;
    }

    public String getMblnr() {
        return mblnr;
    }

    public void setMblnr(String mblnr) {
        this.mblnr = mblnr;
    }

    public String getMjahr() {
        return mjahr;
    }

    public void setMjahr(String mjahr) {
        this.mjahr = mjahr;
    }

    public MarkingStatusesChoices getDocStatus() {
        return docStatus;
    }

    public void setDocStatus(MarkingStatusesChoices docStatus) {
        this.docStatus = docStatus;
    }
}
