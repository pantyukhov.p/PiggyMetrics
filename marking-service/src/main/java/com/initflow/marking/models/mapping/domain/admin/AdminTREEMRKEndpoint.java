package com.initflow.marking.models.mapping.domain.admin;

import com.initflow.marking.domain.marking.MarkingTreemrkStatuses;
import com.initflow.marking.domain.marking.MarkingTypeOfPackaging;

public class AdminTREEMRKEndpoint {

    private Long id;

    private MarkingTypeOfPackaging prnttype = MarkingTypeOfPackaging.CONSUMER;
    private MarkingTypeOfPackaging currentChldtype = MarkingTypeOfPackaging.CONSUMER;
    private MarkingTreemrkStatuses status = MarkingTreemrkStatuses.EMITTED;
    private String value;
    private Long treemrkId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public MarkingTypeOfPackaging getPrnttype() {
        return prnttype;
    }

    public void setPrnttype(MarkingTypeOfPackaging prnttype) {
        this.prnttype = prnttype;
    }

    public MarkingTypeOfPackaging getCurrentChldtype() {
        return currentChldtype;
    }

    public void setCurrentChldtype(MarkingTypeOfPackaging currentChldtype) {
        this.currentChldtype = currentChldtype;
    }

    public MarkingTreemrkStatuses getStatus() {
        return status;
    }

    public void setStatus(MarkingTreemrkStatuses status) {
        this.status = status;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Long getTreemrkId() {
        return treemrkId;
    }

    public void setTreemrkId(Long treemrkId) {
        this.treemrkId = treemrkId;
    }
}
