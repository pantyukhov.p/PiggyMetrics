package com.initflow.marking.controller.admin.domain;

import com.initflow.base.controller.AbstractCRUDController;
import com.initflow.marking.domain.marking.MRKH;
import com.initflow.marking.mapper.domain.admin.AdminMRKHCrudMapper;
import com.initflow.marking.models.mapping.domain.admin.AdminMRKHEndpoint;
import com.initflow.marking.service.domain.MRKHService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.initflow.base.constants.AuthRoles.ADMIN;
import static com.initflow.base.constants.AuthRoles.USER;

@RestController
@RequestMapping("/admin/mrkh")
@PreAuthorize("hasAnyAuthority('"+ ADMIN+ "')")
public class MRKHAdminController extends AbstractCRUDController<MRKH, AdminMRKHEndpoint, AdminMRKHEndpoint,
        AdminMRKHEndpoint, Long> {

    private MRKHService mrkhService;
    private AdminMRKHCrudMapper docHCrudMapper;

    @Autowired
    public MRKHAdminController(MRKHService mrkhService, AdminMRKHCrudMapper crudMapper){
        super(mrkhService, crudMapper);
        this.mrkhService = mrkhService;
        this.docHCrudMapper = crudMapper;
    }

    @Override
    public String[] getReadRoles() {
        return new String[]{ADMIN, USER};
    }

    @Override
    public String[] getUpdateRoles() {
        return new String[]{ADMIN, USER};
    }

    @Override
    public String[] getCreateRoles() {
        return new String[]{ADMIN, USER};
    }
}
