package com.initflow.marking.controller.admin.domain;

import com.initflow.base.controller.AbstractCRUDController;
import com.initflow.marking.domain.marking.TREEMRK;
import com.initflow.marking.mapper.domain.admin.AdminTreeMRKCrudMapper;
import com.initflow.marking.models.mapping.domain.admin.AdminTREEMRKEndpoint;
import com.initflow.marking.service.domain.TREEMRKService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.initflow.base.constants.AuthRoles.ADMIN;
import static com.initflow.base.constants.AuthRoles.USER;


@RestController
@RequestMapping("/admin/mrk_tree")
@PreAuthorize("hasAnyAuthority('"+ ADMIN+ "')")
public class MRKTreeAdminController extends AbstractCRUDController<TREEMRK, AdminTREEMRKEndpoint, AdminTREEMRKEndpoint,
        AdminTREEMRKEndpoint, Long> {

    private TREEMRKService treemrkService;
    private AdminTreeMRKCrudMapper docHCrudMapper;

    @Autowired
    public MRKTreeAdminController(TREEMRKService treemrkService, AdminTreeMRKCrudMapper crudMapper){
        super(treemrkService, crudMapper);
        this.treemrkService = treemrkService;
        this.docHCrudMapper = crudMapper;
    }

    @Override
    public String[] getReadRoles() {
        return new String[]{ADMIN, USER};
    }

    @Override
    public String[] getUpdateRoles() {
        return new String[]{ADMIN, USER};
    }

    @Override
    public String[] getCreateRoles() {
        return new String[]{ADMIN, USER};
    }
}