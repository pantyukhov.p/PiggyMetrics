package com.initflow.marking.controller.admin.document;

import com.initflow.base.models.endpoints.mrkdoc.MRKDocEqualStatus;
import com.initflow.base.models.endpoints.mrkdoc.MRKPDocEndpoint;
import com.initflow.marking.client.DocumentClientService;
import com.initflow.marking.domain.marking.MRKH;
import com.initflow.marking.domain.marking.MRKP;
import com.initflow.marking.domain.marking.MRKPStatusesChoices;
import com.initflow.marking.domain.marking.MarkingStatusesChoices;
import com.initflow.marking.mapper.client.MRKPDocEndpointMapper;
import com.initflow.marking.service.domain.MRKHService;
import com.initflow.marking.service.domain.MRKPService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import java.util.List;

import static com.initflow.base.constants.AuthRoles.ADMIN;

@RestController
@RequestMapping("/admin/mrk/doc")
@PreAuthorize("hasAnyAuthority('"+ ADMIN+ "')")
public class MRKDocumentAdminController {

    @Autowired
    private MRKPService mrkpService;
    @Autowired
    private DocumentClientService documentClientService;
    @Autowired
    private MRKHService mrkhService;
    @Autowired
    private MRKPDocEndpointMapper mrkpDocEndpointMapper;

    @PreAuthorize("hasAnyAuthority('"+ ADMIN+ "')")
    @RequestMapping(value = "/{docnum}/check", method = RequestMethod.POST)
    public @ResponseBody
    ResponseEntity<Boolean> checkCodesMRK(@PathVariable String docnum) {
        List<MRKP> mrkps = mrkpService.getCodeMRKByDocnum(docnum);

        if(mrkps.size() < 1) return new ResponseEntity<>(false, HttpStatus.NOT_FOUND);

        MRKH mrkh = mrkps.get(0).getMrkh();
        List<MRKPDocEndpoint> mrkpDocs = mrkpDocEndpointMapper.map(mrkps);
        MRKDocEqualStatus status = documentClientService.checkEqualCodesMrk(mrkpDocs).getBody();

        if(status == MRKDocEqualStatus.EQUAL){
            mrkh.setDocStatus(MarkingStatusesChoices.STATUSE_OK);
            mrkps.forEach(it -> it.setPosstatus(MRKPStatusesChoices.MRKP_STATUSE_OK));
        } else if(status == MRKDocEqualStatus.MRKP_MORE_ELEMETS) {
            mrkh.setDocStatus(MarkingStatusesChoices.STATUSE_ERROR);
            mrkps.forEach(it -> it.setPosstatus(MRKPStatusesChoices.MRKP_STATUSE_ERROR));
        } else {
            mrkh.setDocStatus(MarkingStatusesChoices.STATUSE_ERROR);
        }

        mrkhService.save(mrkh);
        mrkpService.saveAll(mrkps);

        return ResponseEntity.ok(true);
    }
}