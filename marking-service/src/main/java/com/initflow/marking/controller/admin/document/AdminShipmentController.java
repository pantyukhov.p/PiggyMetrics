package com.initflow.marking.controller.admin.document;

import com.initflow.base.util.map.CastEndpointPageUtil;
import com.initflow.marking.domain.marking.MRKH;
import com.initflow.marking.mapper.domain.MRKHEndpointMapper;
import com.initflow.marking.models.mapping.domain.MRKHEndpoint;
import com.initflow.marking.service.domain.MRKHService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import static com.initflow.base.constants.AuthRoles.ADMIN;

@RestController
@RequestMapping("/admin/mrk/doc")
@PreAuthorize("hasAnyAuthority('"+ ADMIN+ "')")
public class AdminShipmentController {

    @Autowired
    private MRKHService mrkhService;
    @Autowired
    private MRKHEndpointMapper mrkhEndpointMapper;

    @RequestMapping(value = "/shipment", method = RequestMethod.GET)
    public @ResponseBody
    Page<MRKHEndpoint> shipmentLoad(@RequestParam String ebeln, @RequestParam String vbeln,
                                    @RequestParam String mblnr, @RequestParam String mjahr, Pageable pageable) {
        Page<MRKH> mrkhs = mrkhService.getByParams(ebeln, vbeln, mblnr, mjahr, pageable);
        return CastEndpointPageUtil.map(mrkhEndpointMapper, mrkhs, pageable);

    }
}
