package com.initflow.marking.controller.admin.domain;

import com.initflow.base.controller.AbstractCRUDController;
import com.initflow.marking.domain.marking.MRKP;
import com.initflow.marking.mapper.domain.admin.AdminMRKPCrudMapper;
import com.initflow.marking.models.mapping.domain.admin.AdminMRKPEndpoint;
import com.initflow.marking.service.domain.MRKPService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.initflow.base.constants.AuthRoles.ADMIN;
import static com.initflow.base.constants.AuthRoles.USER;


@RestController
@RequestMapping("/admin/mrkp")
@PreAuthorize("hasAnyAuthority('"+ ADMIN+ "')")
public class MRKPAdminController extends AbstractCRUDController<MRKP, AdminMRKPEndpoint, AdminMRKPEndpoint,
        AdminMRKPEndpoint, Long> {

    private MRKPService mrkpService;
    private AdminMRKPCrudMapper docHCrudMapper;

    @Autowired
    public MRKPAdminController(MRKPService mrkpService, AdminMRKPCrudMapper crudMapper){
        super(mrkpService, crudMapper);
        this.mrkpService = mrkpService;
        this.docHCrudMapper = crudMapper;
    }

    @Override
    public String[] getReadRoles() {
        return new String[]{ADMIN, USER};
    }

    @Override
    public String[] getUpdateRoles() {
        return new String[]{ADMIN, USER};
    }

    @Override
    public String[] getCreateRoles() {
        return new String[]{ADMIN, USER};
    }
}