package com.initflow.marking.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/common")
public class CommonController {


	@RequestMapping("/hello")
	@PreAuthorize("hasAnyAuthority('ROLE_USER')")
	public String hello() {
		return "welcome";
	}

	@RequestMapping("/hello_adm")
	@PreAuthorize("hasAnyAuthority('ROLE_ADMIN')")
	public String helloAdm() {
		return "welcome Admin";
	}


}
