package com.initflow.marking.repository;

import com.initflow.marking.domain.marking.TREEMRK;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TREEMRKRepository extends PagingAndSortingRepository<TREEMRK, Long> {
}