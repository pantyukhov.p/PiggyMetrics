package com.initflow.marking.repository;

import com.initflow.marking.domain.marking.MRKP;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MRKPRepository extends PagingAndSortingRepository<MRKP, Long> {

    @Query("select mrkp from MRKP mrkp where mrkp.mrkh.docnum = :docnum")
    List<MRKP> getCodeMRKByDocnum(@Param("docnum") String docnum);

    @Query("select mrkp from MRKP mrkp where mrkp.mrkh.ebeln = :ebeln and mrkp.mrkh.vbeln = :vbeln " +
            " and mrkp.mrkh.mblnr = :mblnr and mrkp.mrkh.mjahr = :mjahr")
    Page<MRKP> getByParams(@Param("ebeln") String ebeln, @Param("vbeln") String vbeln, @Param("mblnr") String mblnr,
                           @Param("mjahr") String mjahr, Pageable pageable);
}