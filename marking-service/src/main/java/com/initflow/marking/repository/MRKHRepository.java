package com.initflow.marking.repository;

import com.initflow.marking.domain.marking.MRKH;
import com.initflow.marking.domain.marking.MRKP;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface MRKHRepository extends PagingAndSortingRepository<MRKH, Long> {

    @Query("select mrkh from MRKH mrkh where mrkh.ebeln = :ebeln and mrkh.vbeln = :vbeln " +
            " and mrkh.mblnr = :mblnr and mrkh.mjahr = :mjahr")
    Page<MRKH> getByParams(@Param("ebeln") String ebeln, @Param("vbeln") String vbeln, @Param("mblnr") String mblnr,
                           @Param("mjahr") String mjahr, Pageable pageable);
}
