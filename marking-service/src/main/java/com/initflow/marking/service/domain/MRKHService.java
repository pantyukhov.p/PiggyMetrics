package com.initflow.marking.service.domain;

import com.initflow.base.service.CrudService;
import com.initflow.marking.domain.marking.MRKH;
import com.initflow.marking.domain.marking.MRKP;
import com.initflow.marking.repository.MRKHRepository;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

@Service
public class MRKHService extends CrudService<MRKH, Long> {

    @Autowired
    private MRKHRepository repository;

    @Override
    protected PagingAndSortingRepository<MRKH, Long> getRepository() {
        return repository;
    }

    @Override
    public MRKH save(MRKH obj){
        checkKey(obj);
        return getRepository().save(obj);
    }

    @Override
    public Iterable<MRKH> saveAll(Iterable<MRKH> objects){
        objects.forEach(this::checkKey);
        return getRepository().saveAll(objects);
    }

    private void checkKey(MRKH obj){
        if(StringUtils.isBlank(obj.getDocnum())){
            obj.setDocnum("uuid_" + UUID.randomUUID().toString());
        }
    }

    @Transactional(readOnly = true)
    public Page<MRKH> getByParams(String ebeln, String vbeln, String mblnr, String mjahr, Pageable pageable){
        Page<MRKH> pages = repository.getByParams(ebeln, vbeln, mblnr, mjahr, pageable);
        pages.getContent().forEach(MRKH::getMrkps);
        return pages;
    }

}