package com.initflow.marking.service.domain;

import com.initflow.base.service.CrudService;
import com.initflow.marking.domain.marking.MRKP;
import com.initflow.marking.repository.MRKPRepository;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class MRKPService extends CrudService<MRKP, Long> {

    @Autowired
    private MRKPRepository repository;

    @Override
    protected PagingAndSortingRepository<MRKP, Long> getRepository() {
        return repository;
    }

    public List<MRKP> getCodeMRKByDocnum(String docnum){
        return repository.getCodeMRKByDocnum(docnum);
    }

    @Override
    public MRKP save(MRKP obj){
        checkKey(obj);
        return getRepository().save(obj);
    }

    @Override
    public Iterable<MRKP> saveAll(Iterable<MRKP> objects){
        objects.forEach(this::checkKey);
        return getRepository().saveAll(objects);
    }

    private void checkKey(MRKP obj){
        if(StringUtils.isBlank(obj.getPosnum())){
            obj.setPosnum("uuid_" + UUID.randomUUID().toString());
        }
    }

    public Page<MRKP> getByParams(String ebeln, String vbeln, String mblnr, String mjahr, Pageable pageable){
        return repository.getByParams(ebeln, vbeln, mblnr, mjahr, pageable);
    }

}