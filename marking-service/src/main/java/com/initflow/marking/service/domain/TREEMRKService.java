package com.initflow.marking.service.domain;

import com.initflow.base.service.CrudService;
import com.initflow.marking.domain.marking.TREEMRK;
import com.initflow.marking.repository.TREEMRKRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Service;

@Service
public class TREEMRKService extends CrudService<TREEMRK, Long> {

    @Autowired
    private TREEMRKRepository repository;

    @Override
    protected PagingAndSortingRepository<TREEMRK, Long> getRepository() {
        return repository;
    }
}