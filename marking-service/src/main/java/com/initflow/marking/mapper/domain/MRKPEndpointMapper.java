package com.initflow.marking.mapper.domain;

import com.initflow.base.mapper.domain.EntityMapper;
import com.initflow.marking.domain.marking.MRKP;
import com.initflow.marking.models.mapping.domain.MRKPEndpoint;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring")
public interface MRKPEndpointMapper extends EntityMapper<MRKPEndpoint, MRKP> {

    @Override
    @Mappings({
            @Mapping(target = "mrkh.id", source = "mrkhId")
    })
    MRKP toEntity(MRKPEndpoint obj);


    @Override
    @Mappings({
            @Mapping(target = "mrkhId", source = "mrkh.id")
    })
    MRKPEndpoint toDto(MRKP obj);

}