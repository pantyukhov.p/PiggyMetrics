package com.initflow.marking.mapper.domain.admin;


import com.initflow.base.mapper.domain.CrudMapper;
import com.initflow.marking.domain.marking.TREEMRK;
import com.initflow.marking.models.mapping.domain.admin.AdminTREEMRKEndpoint;
import org.mapstruct.*;

@Mapper(componentModel = "spring")
public interface AdminTreeMRKCrudMapper extends CrudMapper<TREEMRK, AdminTREEMRKEndpoint, AdminTREEMRKEndpoint,
        AdminTREEMRKEndpoint> {

    @InheritConfiguration
    @Override
    @Mappings({
            @Mapping(target = "parent.id", source = "treemrkId")
    })
    void updateMapping(AdminTREEMRKEndpoint from, @MappingTarget TREEMRK to);

    @Override
    @Mappings({
            @Mapping(target = "parent.id", source = "treemrkId")
    })
    TREEMRK createMapping(AdminTREEMRKEndpoint obj);

    @Override
    @Mappings({
            @Mapping(target = "treemrkId", source = "parent.id")
    })
    AdminTREEMRKEndpoint readMapping(TREEMRK obj);

}
