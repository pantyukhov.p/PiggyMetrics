package com.initflow.marking.mapper.domain.admin;

import com.initflow.base.mapper.domain.CrudMapper;
import com.initflow.marking.domain.marking.MRKH;
import com.initflow.marking.models.mapping.domain.admin.AdminMRKHEndpoint;
import org.mapstruct.InheritConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;

@Mapper(componentModel = "spring")
public interface AdminMRKHCrudMapper extends CrudMapper<MRKH, AdminMRKHEndpoint, AdminMRKHEndpoint, AdminMRKHEndpoint> {

    @InheritConfiguration
    @Override
    void updateMapping(AdminMRKHEndpoint from, @MappingTarget MRKH to);

}