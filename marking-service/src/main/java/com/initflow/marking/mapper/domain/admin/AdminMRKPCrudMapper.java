package com.initflow.marking.mapper.domain.admin;


import com.initflow.base.mapper.domain.CrudMapper;
import com.initflow.marking.domain.marking.MRKP;
import com.initflow.marking.models.mapping.domain.admin.AdminMRKPEndpoint;
import org.mapstruct.*;

@Mapper(componentModel = "spring")
public interface AdminMRKPCrudMapper extends CrudMapper<MRKP, AdminMRKPEndpoint, AdminMRKPEndpoint, AdminMRKPEndpoint> {

    @InheritConfiguration
    @Override
    @Mappings({
            @Mapping(target = "mrkh.id", source = "mrkhId")
    })
    void updateMapping(AdminMRKPEndpoint from, @MappingTarget MRKP to);

    @Override
    @Mappings({
            @Mapping(target = "mrkh.id", source = "mrkhId")
    })
    MRKP createMapping(AdminMRKPEndpoint obj);

    @Override
    @Mappings({
            @Mapping(target = "mrkhId", source = "mrkh.id")
    })
    AdminMRKPEndpoint readMapping(MRKP obj);
}
