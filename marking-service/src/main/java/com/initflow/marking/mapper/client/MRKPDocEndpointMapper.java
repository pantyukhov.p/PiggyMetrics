package com.initflow.marking.mapper.client;

import com.initflow.base.models.endpoints.mrkdoc.MRKPDocEndpoint;
import com.initflow.marking.domain.marking.MRKP;
import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface MRKPDocEndpointMapper {

    MRKPDocEndpoint map(MRKP obj);

//    @IterableMapping
    List<MRKPDocEndpoint> map(List<MRKP> objs);

}