package com.initflow.marking.mapper.domain;

import com.initflow.base.mapper.domain.EntityMapper;
import com.initflow.marking.domain.marking.MRKH;
import com.initflow.marking.models.mapping.domain.MRKHEndpoint;
import com.initflow.marking.models.mapping.domain.admin.AdminMRKHEndpoint;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = MRKPEndpointMapper.class)
public interface MRKHEndpointMapper extends EntityMapper<MRKHEndpoint, MRKH> {


}