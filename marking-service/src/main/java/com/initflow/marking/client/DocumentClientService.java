package com.initflow.marking.client;

import com.initflow.base.models.endpoints.mrkdoc.MRKDocEqualStatus;
import com.initflow.base.models.endpoints.mrkdoc.MRKPDocEndpoint;
import feign.RequestLine;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@FeignClient(name = "document-service")
public interface DocumentClientService {

    @RequestMapping(method = RequestMethod.POST, value = "/document/admin/doc/mrk/check",
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    ResponseEntity<MRKDocEqualStatus> checkEqualCodesMrk(List<MRKPDocEndpoint> codesMrk);

}