package com.initflow.marking.domain.marking;

public enum MarkingStatusesChoices {

    STATUSE_ERROR("Расхождения"), STATUSE_OK("ОК");

    private String value;

    MarkingStatusesChoices(String value){
        this.value = value;
    }

    public String getValue(){
        return value;
    }
}
