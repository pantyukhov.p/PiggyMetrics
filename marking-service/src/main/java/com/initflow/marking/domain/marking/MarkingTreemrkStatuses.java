package com.initflow.marking.domain.marking;

public enum MarkingTreemrkStatuses {
    EMITTED("Расхождения"), APPLIED("Нанесён"), INTRODUCED("Введён в оборот"),
    WRITTEN_OFF("Выведен из оборота, списан"), WITHDRAWN("Выведен из оборота, продан"),
    UNDEFINED("Неопределен");

    private String value;

    MarkingTreemrkStatuses(String value){
        this.value = value;
    }

    public String getValue(){
        return value;
    }
}
