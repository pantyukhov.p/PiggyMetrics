package com.initflow.marking.domain.marking;

public enum MarkingTypeOfPackaging {
    CONSUMER(1, "Потребительская (CIS)"), GROUP(2, "Групповая(CIS)"),
    TRANSPORTATION(3,"Tранспортная(SSCC)");

    private String value;
    private int index;

    MarkingTypeOfPackaging(int index, String value){
        this.index = index;
        this.value = value;
    }

    public int getIndex() {
        return index;
    }

    public String getValue() {
        return value;
    }
}
