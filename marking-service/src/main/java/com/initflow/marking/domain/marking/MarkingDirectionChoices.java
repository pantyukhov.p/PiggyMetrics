package com.initflow.marking.domain.marking;

public enum MarkingDirectionChoices {
    IN("Входящий"), OUT("Исходящий");

    private String value;

    MarkingDirectionChoices(String value){
        this.value = value;
    }

    public String getValue(){
        return value;
    }
}
