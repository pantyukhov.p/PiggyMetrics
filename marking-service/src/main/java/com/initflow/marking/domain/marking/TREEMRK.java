package com.initflow.marking.domain.marking;

import com.initflow.base.models.domain.BaseEntity;

import javax.persistence.*;

@Entity
@Table(name = "mrk_tree")
public class TREEMRK extends BaseEntity {

    @Column(nullable = false, columnDefinition = "varchar(30) default CONSUMER")
    @Enumerated(EnumType.STRING)
    private MarkingTypeOfPackaging prnttype = MarkingTypeOfPackaging.CONSUMER;
    @Column(nullable = false, columnDefinition = "varchar(30) default CONSUMER")
    @Enumerated(EnumType.STRING)
    private MarkingTypeOfPackaging currentChldtype = MarkingTypeOfPackaging.CONSUMER;
    @Column(nullable = false, columnDefinition = "varchar(30) default EMITTED")
    @Enumerated(EnumType.STRING)
    private MarkingTreemrkStatuses status = MarkingTreemrkStatuses.EMITTED;
    @Column(length = 100)
    private String value;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "parent_id")
    private TREEMRK parent;

    public MarkingTypeOfPackaging getPrnttype() {
        return prnttype;
    }

    public void setPrnttype(MarkingTypeOfPackaging prnttype) {
        this.prnttype = prnttype;
    }

    public MarkingTypeOfPackaging getCurrentChldtype() {
        return currentChldtype;
    }

    public void setCurrentChldtype(MarkingTypeOfPackaging currentChldtype) {
        this.currentChldtype = currentChldtype;
    }

    public MarkingTreemrkStatuses getStatus() {
        return status;
    }

    public void setStatus(MarkingTreemrkStatuses status) {
        this.status = status;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public TREEMRK getParent() {
        return parent;
    }

    public void setParent(TREEMRK parent) {
        this.parent = parent;
    }
}
