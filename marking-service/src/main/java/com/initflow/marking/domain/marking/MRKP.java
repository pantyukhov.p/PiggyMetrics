package com.initflow.marking.domain.marking;

import com.initflow.base.models.domain.BaseEntity;

import javax.persistence.*;

@Entity
@Table(name = "mrk_p", indexes = {@Index(name = "mrk_p_posnum_index", columnList = "posnum", unique = true)})
public class MRKP extends BaseEntity {

    @Column(length = 50, unique = true)
    private String posnum;
    @Column(length = 150)
    private String codemrk;
    @Column(nullable = false, columnDefinition = "varchar(30) default CONSUMER")
    @Enumerated(EnumType.STRING)
    private MarkingTypeOfPackaging mrktype = MarkingTypeOfPackaging.CONSUMER;
    @Column(length = 20)
    private String ebeln;
    @Column
    private Integer ebelp;
    @Column(length = 20)
    private String vbeln;
    @Column
    private Integer posnr;
    @Column(length = 20)
    private String mblnr;
    @Column
    private Integer mjahr;
    @Column(nullable = false, columnDefinition = "varchar(30) default MRKP_STATUSE_OK")
    @Enumerated(EnumType.STRING)
    private MRKPStatusesChoices posstatus = MRKPStatusesChoices.MRKP_STATUSE_OK;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "mrkh_id")
    private MRKH mrkh;

    public String getPosnum() {
        return posnum;
    }

    public void setPosnum(String posnum) {
        this.posnum = posnum;
    }

    public String getCodemrk() {
        return codemrk;
    }

    public void setCodemrk(String codemrk) {
        this.codemrk = codemrk;
    }

    public MarkingTypeOfPackaging getMrktype() {
        return mrktype;
    }

    public void setMrktype(MarkingTypeOfPackaging mrktype) {
        this.mrktype = mrktype;
    }

    public String getEbeln() {
        return ebeln;
    }

    public void setEbeln(String ebeln) {
        this.ebeln = ebeln;
    }

    public Integer getEbelp() {
        return ebelp;
    }

    public void setEbelp(Integer ebelp) {
        this.ebelp = ebelp;
    }

    public String getVbeln() {
        return vbeln;
    }

    public void setVbeln(String vbeln) {
        this.vbeln = vbeln;
    }

    public Integer getPosnr() {
        return posnr;
    }

    public void setPosnr(Integer posnr) {
        this.posnr = posnr;
    }

    public String getMblnr() {
        return mblnr;
    }

    public void setMblnr(String mblnr) {
        this.mblnr = mblnr;
    }

    public Integer getMjahr() {
        return mjahr;
    }

    public void setMjahr(Integer mjahr) {
        this.mjahr = mjahr;
    }

    public MRKPStatusesChoices getPosstatus() {
        return posstatus;
    }

    public void setPosstatus(MRKPStatusesChoices posstatus) {
        this.posstatus = posstatus;
    }

    public MRKH getMrkh() {
        return mrkh;
    }

    public void setMrkh(MRKH mrkh) {
        this.mrkh = mrkh;
    }
}
