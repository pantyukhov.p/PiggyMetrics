package com.initflow.marking.domain.marking;

public enum MRKPStatusesChoices {
    MRKP_STATUSE_ERROR("Отсутствует в УПД"), MRKP_STATUSE_OK("Ок");

    private String value;

    MRKPStatusesChoices(String value){
        this.value = value;
    }

    public String getValue(){
        return value;
    }
}
