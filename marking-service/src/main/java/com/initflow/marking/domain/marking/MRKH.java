package com.initflow.marking.domain.marking;


import com.initflow.base.models.domain.BaseEntity;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "mrk_h", indexes = {@Index(name = "mrk_h_docnum_index", columnList = "docnum", unique = true)})
public class MRKH extends BaseEntity {

    @Column(nullable = false, unique = true, length = 50)
    private String docnum;
    @Column(nullable = false, columnDefinition = "varchar(30) default IN")
    @Enumerated(EnumType.STRING)
    private MarkingDirectionChoices direction = MarkingDirectionChoices.IN;
    @Column(columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    private LocalDateTime docDate;
    @Column
    private String lifnr;
    @Column
    private String ebeln;
    @Column
    private String vbeln;
    @Column
    private String mblnr;
    @Column
    private String mjahr;
    @Column(nullable = false, columnDefinition = "varchar(30) default STATUSE_OK")
    @Enumerated(EnumType.STRING)
    private MarkingStatusesChoices docStatus = MarkingStatusesChoices.STATUSE_OK;
    @OneToMany(mappedBy = "mrkh")
    private List<MRKP> mrkps = new ArrayList<>();


    public MarkingDirectionChoices getDirection() {
        return direction;
    }

    public void setDirection(MarkingDirectionChoices direction) {
        this.direction = direction;
    }

    public LocalDateTime getDocDate() {
        return docDate;
    }

    public void setDocDate(LocalDateTime docDate) {
        this.docDate = docDate;
    }

    public String getLifnr() {
        return lifnr;
    }

    public void setLifnr(String lifnr) {
        this.lifnr = lifnr;
    }

    public String getEbeln() {
        return ebeln;
    }

    public void setEbeln(String ebeln) {
        this.ebeln = ebeln;
    }

    public String getVbeln() {
        return vbeln;
    }

    public void setVbeln(String vbeln) {
        this.vbeln = vbeln;
    }

    public String getMblnr() {
        return mblnr;
    }

    public void setMblnr(String mblnr) {
        this.mblnr = mblnr;
    }

    public String getMjahr() {
        return mjahr;
    }

    public void setMjahr(String mjahr) {
        this.mjahr = mjahr;
    }

    public MarkingStatusesChoices getDocStatus() {
        return docStatus;
    }

    public void setDocStatus(MarkingStatusesChoices docStatus) {
        this.docStatus = docStatus;
    }

    public String getDocnum() {
        return docnum;
    }

    public void setDocnum(String docnum) {
        this.docnum = docnum;
    }

    public List<MRKP> getMrkps() {
        return mrkps;
    }

    public void setMrkps(List<MRKP> mrkps) {
        this.mrkps = mrkps;
    }
}
