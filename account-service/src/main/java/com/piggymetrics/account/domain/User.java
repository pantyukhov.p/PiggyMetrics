package com.piggymetrics.account.domain;

import javax.persistence.*;

@Entity
@Table(name = "mtr_user")
public class User {

	@Id
	private String username;

	private String password;

	public String getPassword() {
		return password;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}