package com.piggymetrics.account.domain;

import org.hibernate.validator.constraints.Length;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;


@Entity
@Table(name = "mtr_account")
public class Account {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(unique = true)
	private String name;

	@Column
	private Date lastSeen;

	@Length(min = 0, max = 20_000)
	private String note;

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "account_company",
			joinColumns = @JoinColumn(name="account_id", referencedColumnName="id"),
			inverseJoinColumns = @JoinColumn(name="company_id", referencedColumnName="id"))
	private Set<Company> companies = new HashSet<>();


	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getLastSeen() {
		return lastSeen;
	}

	public void setLastSeen(Date lastSeen) {
		this.lastSeen = lastSeen;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Set<Company> getCompanies() {
		return companies;
	}

	public void setCompanies(Set<Company> companies) {
		this.companies = companies;
	}

}
