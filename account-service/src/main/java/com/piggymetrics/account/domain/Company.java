package com.piggymetrics.account.domain;

import com.initflow.base.models.domain.BaseEntity;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "acc_company", indexes = {@Index(name = "acc_company_uniq_name_index", columnList = "uniq_name", unique = true)})
public class Company extends BaseEntity {

    @Column(name = "uniq_name", unique = true, nullable = false)
    private String uniqName;
    @Column(name = "name", nullable = false)
    private String name;

//    @ManyToMany(mappedBy = "companies")
//    private Set<Account> accounts = new HashSet<>();

    public String getUniqName() {
        return uniqName;
    }

    public void setUniqName(String uniqName) {
        this.uniqName = uniqName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

//    public void setAccounts(Set<Account> users) {
//        this.accounts = users;
//    }

//    public Set<Account> getAccounts(){
//        return accounts;
//    }
}

